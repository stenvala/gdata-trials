﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TrialApp.Services;
using TrialApp.Models;
using rest.Models;

// cd live/trials-gdata/back; sudo chown -R www-data:www-data *; cd ~

namespace TrialApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RaceController : ControllerBase
    {
        private readonly IRaceIndex RaceIndex;
        private readonly ICache Cache;
        private readonly IRaceSheetFactory RaceSheetFactory;

        public RaceController(IRaceIndex raceIndex,
            IRaceSheetFactory raceSheetFactory,
            ICache cache)
        {            
            RaceIndex = raceIndex;
            RaceSheetFactory = raceSheetFactory;
            Cache = cache;
        }

        [HttpGet("clear")]
        public JsonResult GetClearCache()
        {
            Cache.Clear();
            return new JsonResult("ok");
        }

        [HttpGet("index/clear")]
        public JsonResult GetClearIndex()
        {
            RaceIndex.ClearCache();
            return new JsonResult("ok");
        }        

        [HttpGet("index")]
        public JsonResult GetIndex()
        {
            return new JsonResult(RaceIndex.Get());
        }

        [HttpGet("clear-races")]
        public JsonResult GetClearRaces()
        {
            RaceSheetFactory.ClearCaches();
            return new JsonResult("ok");
        }

        [HttpGet("{sheetId}/clear")]
        public JsonResult GetClearRaceCache(string sheetId)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            race.ClearCache();
            return new JsonResult("ok");
        }

        [HttpGet("{sheetId}/definition")]
        public JsonResult GetDefinition(string sheetId)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.GetDefinition());
        }

        [HttpGet("{sheetId}/categories")]
        public JsonResult GetCategories(string sheetId)        
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.GetCategories());
        }

        [HttpGet("{sheetId}/riders")]
        public JsonResult GetRiders(string sheetId)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.GetRiders());
        }

        [HttpGet("{sheetId}/category/{category}")]
        public JsonResult GetCategory(string sheetId, string category)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.GetCategory(category));
        }

        [HttpGet("{sheetId}/category/{category}/riders")]
        public JsonResult GetRidersOfCategory(string sheetId, string category)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.GetRidersOf(category));
        }

        [HttpPost("{sheetId}/refereeing/solve-section")]
        public JsonResult PostSolveSectionForCode([FromBody]RefereeDTOInBound value, string sheetId)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            var result = new RefereeDTOOutBound()
            {
                Section = race.SolveSectionForReferee(value.Code)
            };
            return new JsonResult(result);
        }

        [HttpPost("{sheetId}/refereeing/save-result")]
        public JsonResult PostSaveSectionResult([FromBody]List<SaveResultDTO> value, string sheetId)
        {
            if (!RaceIndex.CanRefereeRace(sheetId))
            {
                throw new Exception("Can't edit results of race");
            }            
            IRaceSheet race = RaceSheetFactory.Create(sheetId);                        
            race.SaveResult(value);
            return new JsonResult("ok");
        }

        [HttpPost("{sheetId}/queuing")]
        public JsonResult PostTakeRiderUnderControl([FromBody]QueueDTO value, string sheetId)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);            
            var reply = new Dictionary<string, string>{
                { "code", race.TakeRiderQueueToControl(value)}
            };
            return new JsonResult(reply);
        }

        [HttpGet("{sheetId}/queuing")]
        public JsonResult GetQueue(string sheetId)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);            
            return new JsonResult(race.GetQueueData());
        }

        [HttpGet("{sheetId}/queuing/queue-lengths")]
        public JsonResult GetQueueLengths(string sheetId)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.GetQueueLengths());
        }

        [HttpGet("{sheetId}/queuing/of-section/{section}")]
        public JsonResult GetQueueOfSection(string sheetId, int section)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.GetQueueOfSection(section));
        }

        [HttpGet("{sheetId}/queuing/save")]
        public JsonResult GetSaveQueue(string sheetId)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            race.UploadQueue();
            return new JsonResult(race.GetQueueData());
        }

        [HttpGet("{sheetId}/queuing/reset-empty")]
        public JsonResult GetResetEmpty(string sheetId)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);            
            return new JsonResult(race.RemoveUntakenRiders());
        }

        [HttpPut("{sheetId}/queuing/remove-by-referee")]
        public JsonResult RemoveRiderFromQueueByReferee(string sheetId, [FromBody]RefereeRemoveRiderFromQueueDTO data)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            race.RemoveRiderFromQueueByReferee(data);
            return new JsonResult("ok");
        }

        [HttpDelete("{sheetId}/queuing/{code}")]
        public JsonResult DeleteReleaseRider(string sheetId, string code)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            race.ReleaseQueuedRider(code);
            return new JsonResult("ok");
        }

        [HttpGet("{sheetId}/queuing/{code}/result")]
        public JsonResult GetResultAndQueueStatus(string sheetId, string code)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.GetResultAndQueueStatusForCode(code));            
        }

        [HttpPut("{sheetId}/queuing/{code}/section/{section}")]
        public JsonResult GetSelectSection(string sheetId, string code, int section)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.QueueToSectionByCode(code, section));
        }

        [HttpDelete("{sheetId}/queuing/{code}/section")]
        public JsonResult DeleteRiderInQueue(string sheetId, string code)
        {
            IRaceSheet race = RaceSheetFactory.Create(sheetId);
            return new JsonResult(race.RemoveFromQueueByCode(code));
        }

    }
}
