﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TrialApp.Services;
using TrialApp.Models;

namespace TrialApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocController : ControllerBase
    {

        public DocController()
        {

        }

        [HttpGet("temp")]
        public JsonResult GetTempDocument()
        {
            var docReader = new GoogleDocsReader("1d7ZihtdiwgEDGr3Uy66hrQfYH48MvWv0-_Xsc7VjmBU");
            var doc = docReader.GetDocument();
            return new JsonResult(doc.Body);
        }
    }
}
