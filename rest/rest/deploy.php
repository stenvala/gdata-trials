<?php
/*
  Deploy trials-gdata to backend
  build: dotnet publish -c Release
  deploy: php deploy.php
*/

date_default_timezone_set('UTC');

$ts = date('YmdHis');
$base = '~/live/trials-gdata';
$builds = $base . '/back';
$build = $builds . '/' . $ts;
$server = 'stenvala@mathcodingclub.com';
$ssh = 'ssh ' . $server;

// Copy build files
$cmd = $ssh . ' "mkdir ' . $build . '"';
shell_exec($cmd);
$cmd = 'rsync -r token.json ' . $server . ':' . $build;
shell_exec($cmd);
print "Released to $ts\n";

// Copy token.json
$cmd = 'rsync -r bin/Release/netcoreapp2.1/publish/* ' . $server . ':' . $build;
shell_exec($cmd);

// Change link
$symlink = 'current-back';
$cmd = $ssh . ' "rm ' . $base . '/' . $symlink . '"';
shell_exec($cmd);
$cmd = $ssh . ' "ln -s ' . $builds . '/' . $ts . ' ' . $base . '/' . $symlink . '"';
shell_exec($cmd);
print "Changed symlink to $ts\n";

// Remove old builds
$cmd = 'ssh -tt ' . $server . ' "sudo /bin/chown -R stenvala:stenvala ' . $builds . '"';
shell_exec($cmd);
$cmd = $ssh . ' "cd ' . $builds . '; ls"';
$ls = shell_exec($cmd);
$dirs = explode(PHP_EOL, $ls);
while (count($dirs) > 10){  
  $temp = array_shift($dirs);
  $cmd = $ssh . ' "rm -rf ' . $builds . '/' . $temp . '"';
  shell_exec($cmd);  
  print "Removed old release $temp\n";
}

// Modify build
$cmd = 'ssh -tt ' . $server . ' "sudo /bin/chown -R www-data:www-data ' . $build . '"';
shell_exec($cmd);
print "Changed directory access rights\n";

// Restart
$cmd = 'ssh -tt ' . $server . ' "sudo /bin/systemctl restart dotnet-trials-gdata.service"';
shell_exec($cmd);
print "Restarted service\n";
