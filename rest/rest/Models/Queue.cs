﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TrialApp.Services;

namespace TrialApp.Models
{    

    public class QueueStatusAndResultDTO : Rider
    {
        public int QueueInSection;
        public int AheadInQueue;
    }

    public class RefereeRemoveRiderFromQueueDTO
    {
        public string Category;
        public string Code;
        public string RiderName;
        public int RiderNumber;
    }

    public class SafeQueueDTO
    {
        public string Category;
        public int RiderNumber;
        public string RiderName;
        public DateTime Arrived;
        public int Section;
    }

    public class QueueDTO : SafeQueueDTO
    {        
        public string Code;        
    }

    public class QueueDTOEqualityComparer : IEqualityComparer<QueueDTO>
    {
        public int GetHashCode(QueueDTO a)
        {
            return a.Category.GetHashCode() +
                a.RiderNumber.GetHashCode() +
                a.RiderName.GetHashCode();
            }
        public bool Equals(QueueDTO a, QueueDTO b)
        {
            return a.Category == b.Category &&
                a.RiderNumber == b.RiderNumber &&
            a.RiderName == b.RiderName;

        }
    }

    public class SectionQueuer
    {
        public int Section;
        public DateTime Arrived;
    }   

    public class Queue
    {
        public static readonly string DISPLAY_DATE = "yyyy-MM-ddTHH:mm:ss.fffffffK"; 
        public IDictionary<QueueDTO, string> RiderToCode = new Dictionary<QueueDTO, string>(new QueueDTOEqualityComparer());
        public IDictionary<string, QueueDTO> CodeToRider = new Dictionary<string, QueueDTO>();
        public IDictionary<string, SectionQueuer> CodeToSection = new Dictionary<string, SectionQueuer>();

        private DateTime NextUpdate;

        private Queue()
        {

        }

        public bool ShouldUpdate()
        {
            return ShouldUpdate(100);
        }
        public bool ShouldUpdate(int delayForNext)
        {
            if (NextUpdate < DateTime.Now)
            {
                if (delayForNext > 0)
                {
                    NextUpdate = DateTime.Now.AddSeconds(delayForNext);
                }
                return true;
            }
            return false;
        }

        public void RemoveIfNotIn(Queue queue)
        {
            foreach (QueueDTO rider in RiderToCode.Keys.ToList())
            {
                if (!queue.RiderToCode.ContainsKey(rider))
                {                    
                    var code = RiderToCode[rider];
                    if (CodeToSection.ContainsKey(code))
                    {
                        CodeToSection.Remove(code);
                    }
                    CodeToRider.Remove(code);
                    RiderToCode.Remove(rider);
                }
            }

        }

        public static Queue GenerateFromMatrix(IList<IList<Object>> matrix)
        {
            var queue = new Queue();
            queue.NextUpdate = DateTime.Now.AddSeconds(100);
            if (matrix == null)
            {
                return queue;
            }
            foreach (var row in matrix)
            {
                if (row.Count < 1 ||
                    row[0].ToString() == ""
                    )
                {
                    break;
                }
                if (row.Count < 4 || row[3].ToString() == "")
                {
                    continue;
                }
                var rider = new QueueDTO
                {
                    Category = row[0].ToString(),
                    RiderNumber = Int32.Parse(row[1].ToString()),
                    RiderName = row[2].ToString()
                };
                var code = row[3].ToString();
                queue.RiderToCode.Add(rider, code);
                queue.CodeToRider.Add(code, rider);
                if (row.Count > 4)
                {                    
                    var section = Int32.Parse(row[4].ToString());
                    // This is not so important if we fail here                    
                    queue.CodeToSection.Add(code, new SectionQueuer
                    {
                        Section = section,                        
                        Arrived = (DateTime)row[5]
                    });
                }
            }
            return queue;
        }

        // tells if rider can be taken
        public bool IsRiderAvailable(QueueDTO rider)
        {            
            return !RiderToCode.ContainsKey(rider) ||
                RiderToCode[rider] == "";
        }

        public List<QueueDTO> GetFullData()
        {
            var list = new List<QueueDTO>();            
            foreach (KeyValuePair<QueueDTO, string> kvp in RiderToCode)
            {
                var rider = kvp.Key;
                rider.Code = kvp.Value;
                rider.Section = 0;
                if (CodeToSection.ContainsKey(rider.Code))
                {
                    rider.Section = CodeToSection[rider.Code].Section;
                    rider.Arrived = CodeToSection[rider.Code].Arrived;
                }
                list.Add(rider);
            }
            return list;
        }

        public string PickRider(QueueDTO rider)
        {
            var code = CreateNewCode();
            if (!RiderToCode.ContainsKey(rider))
            {
                RiderToCode.Add(rider, code);
            }
            CodeToRider.Add(code, rider);
            return code;
        }

        public string CreateNewCode()
        {
            var code = Util.RandomString(20);
            while (CodeToRider.ContainsKey(code))
            {
                code = Util.RandomString(20);
            }
            return code;
        }

        public void ReleaseRiderByCode(string code)
        {
            var rider = CodeToRider[code];
            RiderToCode.Remove(rider);
            CodeToRider.Remove(code);
            ReleaseSectionForCode(code);
        }

        public void SetSectionForCode(string code, int section)
        {            
            if (!CodeToRider.ContainsKey(code))
            {
                throw new Exception("Unknown code");
            }
            ReleaseSectionForCode(code);
            CodeToSection.Add(code, new SectionQueuer
            {
                Section = section,
                Arrived = DateTime.Now
            });
        }

        public void ReleaseSectionForCode(string code, int section)
        {
            if (CodeToSection.ContainsKey(code) &&
                CodeToSection[code].Section == section)
            {
                CodeToSection.Remove(code);
            }
        }

        public bool IsQueueing(string code)
        {
            return CodeToSection.ContainsKey(code);
        }

        public void ReleaseSectionForCode(string code)
        {
            if (IsQueueing(code))
            {
                CodeToSection.Remove(code);
            }            
        }

        public Dictionary<int, int> GetQueueLengths()
        {
            var lengths = new Dictionary<int, int>();
            foreach (KeyValuePair<string, SectionQueuer> kvp in CodeToSection)
            {
                if (!lengths.ContainsKey(kvp.Value.Section))
                {
                    lengths.Add(kvp.Value.Section, 1);
                } else
                {
                    lengths[kvp.Value.Section]++;
                }
            }
            return lengths;
        }

        public IList<SafeQueueDTO> GetQueueOfSection(int section)
        {
            var queue = new List<SafeQueueDTO>();
            foreach (KeyValuePair<string, SectionQueuer> kvp in CodeToSection)
            {
                if (kvp.Value.Section == section)
                {
                    var rider = CodeToRider[kvp.Key];
                    var publicRider = new SafeQueueDTO
                    {
                        Category = rider.Category,
                        RiderName = rider.RiderName,
                        RiderNumber = rider.RiderNumber,
                        Arrived = kvp.Value.Arrived,
                        Section = section
                    };                                     
                    queue.Add(publicRider);
                }
            }
            return queue;
        }

        public int SolveHowManyAreInFrontOfCode(string code)
        {
            var howMany = 0;
            var my = CodeToSection[code];
            foreach (KeyValuePair<string, SectionQueuer> kvp in CodeToSection)
            {
                if (kvp.Value.Section == my.Section &&
                    kvp.Value.Arrived < my.Arrived)
                {
                    howMany++;
                }
            }
            return howMany;
        }
    }
}
