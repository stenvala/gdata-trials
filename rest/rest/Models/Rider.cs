﻿using System;
using System.Collections.Generic;
namespace TrialApp.Models
{
    public class Rider
    {
        public Dictionary<String,String> RiderData { get; set; } = new Dictionary<String,String>();
        public int Position { get; set; }
        public int Number { get; set; }
        public Dictionary<int, Dictionary<int, int>> SectionResults { get; set; } =
            new Dictionary<int, Dictionary<int, int>>();
        public string Penalties;
        public string Notes;
        public string Time;


        public void SetSectionResult(int lap, int section, int result)
        {
            if (!SectionResults.ContainsKey(lap))
            {
                SectionResults[lap] = new Dictionary<int, int>();
            }
            SectionResults[lap].Add(section, result);
        }

        public static Rider GenerateFromRow(IList<Object> row, Definition def, Category cat, int maxNumberOfSections)
        {            
            var rider = new Rider
            {
                Position = Int32.Parse(row[0].ToString()),
                Number = Int32.Parse(row[1].ToString())                
            };
            for (var i = 0; i < def.RiderColumnNames.Count; i++)
            {             
                rider.RiderData[def.RiderColumnNames[i]] = row[i + 2].ToString();                
            }
            for (var l = 1; l <= cat.Laps; l++)
            {
                rider.SectionResults.Add(l, new Dictionary<int, int>());
                foreach (var s in cat.Sections) {
                    var rowIndex = def.GetFirstColumnForPoints() + (l - 1) * (maxNumberOfSections + 1) + s - 2;
                    var number = row[rowIndex].ToString();
                    if (number != "")
                    {
                        rider.SectionResults[l].Add(s, Int32.Parse(number));
                    }
                }
            }
            var lastLapIndex = def.RiderColumnNames.Count + cat.Laps * (maxNumberOfSections + 1) - 1;
            rider.Time = row[lastLapIndex + 1].ToString();            
            rider.Penalties = row[lastLapIndex + 5].ToString();
            rider.Notes = row[lastLapIndex + 6].ToString();             
            return rider;
        }
    }
}
