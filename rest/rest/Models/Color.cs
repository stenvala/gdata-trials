﻿using System;
using Google.Apis.Sheets.v4.Data;
using Newtonsoft.Json;

namespace TrialApp.Models
{
    public class Color
    {
        [JsonProperty(PropertyName = "R")]
        public int R;
        [JsonProperty(PropertyName = "G")]
        public int G;
        [JsonProperty(PropertyName = "B")]
        public int B;
        public Color(CellData cell)
        {
            R = ParseValue(cell.EffectiveFormat.BackgroundColor.Red);
            G = ParseValue(cell.EffectiveFormat.BackgroundColor.Green);
            B = ParseValue(cell.EffectiveFormat.BackgroundColor.Blue);
        }

        private int ParseValue(float? value)            
        {
            if (value == null)
            {
                return 0;
            }
            return (int)Math.Round((decimal)(255 * value));
        }

    }
}
