﻿using System;
using System.Collections.Generic;
using Google.Apis.Sheets.v4.Data;

namespace rest.Models
{

    public class SaveResultDTO
    {
        public string Category;
        public int RiderNumber;
        public string RiderName;
        public int Section;
        public string Code;
        public int Lap;
        public int Points;
        public bool RemovePoints;
        public string SectionTime;

        public SaveResultDTO()
        {
        }
    }

    public class RefereeDTOInBound
    {
        public string Code;
    }

    public class RefereeDTOOutBound
    {
        public int Section;
    }

    public class RefereeCodes
    {
        private Dictionary<string, int> CodeToSection = new Dictionary<string, int>();
        private Dictionary<int, string> SectionToCode = new Dictionary<int, string>();

        private RefereeCodes()
        {
        }

        public Boolean HasCode(string code)
        {
            return CodeToSection.ContainsKey(code);
        }
        public int GetSectionForCode(string code) => CodeToSection[code];

        public static RefereeCodes GenerateFromMatrix(IList<IList<Object>> matrix)
        {
            var codes = new RefereeCodes();
            foreach (var row in matrix)
            {
                if (row[0].ToString() == "")
                {
                    break;
                }                
                var section = Int32.Parse(row[0].ToString());
                var code = row[1].ToString();
                codes.CodeToSection.Add(code, section);
                codes.SectionToCode.Add(section, code);
            }
            return codes;
        }
    }
}
