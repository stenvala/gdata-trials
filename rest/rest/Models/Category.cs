﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Google.Apis.Sheets.v4.Data;

namespace TrialApp.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PointSystem
    {
        [Description("POINTS")]
        POINTS = 0,

        [Description("PENALTIES")]
        PENALTIES = 1
    }

    public class Category
    {
        public string Name { get; set; }
        public int[] Sections { get; set; }
        public int Laps { get; set; }
        public int FirstNumber { get; set; }
        public int NumberOfRiders { get; set; }
        public PointSystem PointSystem { get; set; }
        public Color Color { get; set; }

        private Category()
        {

        }

        public static Category GenerateFromRow(RowData row) {            
            Category Cat = new Category
            {
                Name = row.Values[0].FormattedValue,
                Laps = Int32.Parse(row.Values[2].FormattedValue),
                FirstNumber = Int32.Parse(row.Values[3].FormattedValue),
                NumberOfRiders = Int32.Parse(row.Values[4].FormattedValue),
                PointSystem = (PointSystem)Enum.Parse(typeof(PointSystem), row.Values[5].FormattedValue),
                Color = new Color(row.Values[6])                
            };            
            string sec = row.Values[1].FormattedValue;
            if (!sec.Contains(","))
            {
                Cat.Sections = Enumerable.Range(1,Int32.Parse(sec)).ToArray();

            } else
            {
                Cat.Sections = Array.ConvertAll<string,int>(sec.Split(','), Int32.Parse);
            }
            return Cat;
        }

        public static int GetMaxNumberOfSectionInCategoryList(IList<Category> cats)
        {
            int max = 0;
            foreach(var cat in cats)
            {
                max = Math.Max(max, cat.Sections.Max());
            }
            return max;
        }
    }
}
