﻿using System;
using System.Collections.Generic;

namespace TrialApp.Models
{
    public class Definition
    {
        public IList<string> RiderColumnNames { get; } = new List<string>();
        public IDictionary<string, string> Translations { get; set; } = new Dictionary<string, string>();
        public string Title { get; set; }
        public string Date { get; set; }  
        public string Location { get; set; }
        public string Organizer { get; set; }
        public int SectionCardRowHeightScale { get; set; }      

        private Definition()
        {

        }

        public int GetFirstColumnForPoints()
        {
            return RiderColumnNames.Count + 3;
        }

        public static Definition GenerateFromMatrix(IList<IList<Object>> matrix)
        {            
            var def = new Definition();            
            def.Translations.Add("Position", matrix[0][0].ToString());
            def.Translations.Add("Number", matrix[0][1].ToString());
            foreach (var cell in matrix[1])
            {
                if (cell.ToString() != "")
                {
                    def.RiderColumnNames.Add(cell.ToString());
                } else
                {
                    break;
                }
            }
            def.Translations.Add("Time", matrix[2][0].ToString());
            def.Translations.Add("Penalty", matrix[3][0].ToString());
            def.Translations.Add("Notes", matrix[4][0].ToString());
            def.Translations.Add("Total", matrix[5][0].ToString());
            def.Translations.Add("Lap", matrix[6][0].ToString());
            def.Title = matrix[15][1].ToString();
            def.Date = matrix[15][2].ToString();
            def.Location = matrix[15][3].ToString();
            def.Organizer = matrix[15][5].ToString();            
            def.SectionCardRowHeightScale = Int32.Parse(matrix[9][0].ToString());
            return def;
        }
    }
}
