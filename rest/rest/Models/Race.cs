﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TrialApp.Models
{

    public class Race
    {
        public string Date { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
        public bool SectionCards { get; set; }
        public bool Refereeing { get; set; }
        public bool Queuing { get; set; }


        private Race()
        {

        }

        public static Race GenerateFromRow(IList<Object> row) {
            return new Race
            {
                Date = row[0].ToString(),
                Name = row[1].ToString(),
                Id = row[2].ToString(),
                SectionCards = row[3].ToString() == "1",
                Refereeing = row[4].ToString() == "1",
                Queuing = row[6].ToString() == "1"
            };
        }
    }
}
