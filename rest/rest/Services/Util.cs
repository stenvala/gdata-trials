﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace TrialApp.Services
{
    public interface IUtil
    {
        string GetIP();        
    }

    public class Util : IUtil
    {

        private static Random random = new Random();

        private IHttpContextAccessor Accessor;

        public Util(IHttpContextAccessor accessor)
        {
            Accessor = accessor;
        }

        public string GetIP()
        {
            return Accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            
        }
        
        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
