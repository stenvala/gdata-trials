﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Google.Apis.Sheets.v4.Data;
    using rest.Models;
    using TrialApp.Models;


    namespace TrialApp.Services
    {

        public interface IRaceSheetFactory
        {
            IRaceSheet Create(string spreadsheetId);
            void ClearCaches();
        }

        public class RaceSheetFactory : IRaceSheetFactory
        {
            private readonly IGoogleSheetReaderFactory SheetReaderFactory;
            private readonly ICache Cache;
            private readonly IUtil Util;

            public RaceSheetFactory(IGoogleSheetReaderFactory readerFactory, ICache cache, IUtil util)
            {
                SheetReaderFactory = readerFactory;
                Cache = cache;
                Util = util;
            }

            public void ClearCaches()
            {
                Cache.Clear(RaceSheet.CacheArea);
            }

            public IRaceSheet Create(string spreadsheetId)
            {
                return new RaceSheet(spreadsheetId,
                    SheetReaderFactory,
                    Cache,                
                    Util);
            }
        }

        public interface IRaceSheet
        {     
            void ClearCache();
            List<Category> GetCategories();
            Category GetCategory(string category);
            Definition GetDefinition();
            List<Rider> GetRidersOf(string category);
            Dictionary<string, List<Rider>> GetRiders();
            int SolveSectionForReferee(string code);
            void SaveResult(List<SaveResultDTO> result);
            string TakeRiderQueueToControl(QueueDTO value);
            void ReleaseQueuedRider(string code);
            List<QueueDTO> GetQueueData();
            void UploadQueue();
            List<QueueDTO> RemoveUntakenRiders();
            QueueStatusAndResultDTO GetResultAndQueueStatusForCode(string code);
            bool QueueToSectionByCode(string code, int number);
            bool RemoveFromQueueByCode(string code);
            Dictionary<int, int> GetQueueLengths();
            IList<SafeQueueDTO> GetQueueOfSection(int section);
            void RemoveRiderFromQueueByReferee(RefereeRemoveRiderFromQueueDTO data);
        }

        public class RaceCacheKeys
        {
            public string Definition
            {
                get
                {
                    return "Definition_" + SheetId;
                }
            }
            public string Categories
            {
                get
                {
                    return "Categories_" + SheetId;
                }
            }
            public string Riders
            {
                get
                {
                    return "Riders_" + SheetId;
                }
            }
            public string RefereeCodes
            {
                get
                {
                    return "RefereeCodes_" + SheetId;
                }
            }
            public string Queue
            {
                get
                {
                    return "Queue_" + SheetId;
                }
            }
            public string SheetIds
            {
                get
                {
                    return "SheetIds_" + SheetId;
                }
            }
            public List<String> All
            {
                get
                {
                    var list = new List<string>() { Categories, Definition, Riders, RefereeCodes, Queue, SheetIds };

                    return list;
                }
            }
            private readonly string SheetId;
            public RaceCacheKeys(string sheetId)
            {
                SheetId = sheetId;
            }
        }

        public class RaceSheet : IRaceSheet
        {
            static public readonly string RefereeCodeSheetId = "1ntdFg7qzf3B_AMLT3Dp5WRMhtiQr8ful0UKbeeCFFtk";
            static public readonly string CacheArea = "RaceData";
            private readonly RaceCacheKeys CacheKeys;        
            private readonly ICache Cache;        
            private readonly IGoogleSheetReader Reader;
            private readonly IGoogleSheetReader RefereeReader;
            private int ActionLogRow = -1;
            private readonly IUtil Util;

            public RaceSheet(string sheetId, IGoogleSheetReaderFactory reader, ICache cache,
                IUtil util)           
            {            
                Reader = reader.Create(sheetId);
                RefereeReader = reader.Create(RaceSheet.RefereeCodeSheetId);
                Cache = cache;
                CacheKeys = new RaceCacheKeys(sheetId);
                Util = util;
            }                

            public void ClearCache()
            {
                Cache.Clear(CacheArea, CacheKeys.All);
            }

            public List<Category> GetCategories()
            {            
                if (Cache.Has(RaceSheet.CacheArea, CacheKeys.Categories))
                {
                    if (!Cache.HasExpired(RaceSheet.CacheArea, CacheKeys.Categories))
                    {
                        var c = (List<Category>)Cache.Get(RaceSheet.CacheArea, CacheKeys.Categories);
                        return c;
                    }
                    Cache.ExtendExpiration(RaceSheet.CacheArea, CacheKeys.Categories, 20);                
                }
                GridData values = Reader.GetGridDataForRange("Definitions!A2:G13");
                var cats = new List<Category>();
                foreach (var row in values.RowData)
                {
                    var cat = Category.GenerateFromRow(row);
                    if (cat.NumberOfRiders > 0)
                    {
                        cats.Add(cat);
                    }
                }
                Cache.Add(RaceSheet.CacheArea, CacheKeys.Categories, cats, 130);
                return cats;
            }        

            public Category GetCategory(string category)
            {
                var cats = GetCategories();
                var cat = cats.Find(c => c.Name == category);
                if (cat == null)
                {
                    throw new Exception("Category not found");
                }
                return cat;
            }

            public Definition GetDefinition()
            {            
                if (Cache.Has(RaceSheet.CacheArea, CacheKeys.Definition))
                {
                    if (!Cache.HasExpired(RaceSheet.CacheArea, CacheKeys.Definition))
                    {
                        return (Definition)Cache.Get(RaceSheet.CacheArea, CacheKeys.Definition);
                    }
                    Cache.ExtendExpiration(RaceSheet.CacheArea, CacheKeys.Definition, 20);
                }
                IList<IList<Object>> values = Reader.GetRange("Definitions!B20:G35");
                var def = Definition.GenerateFromMatrix(values);
                Cache.Add(RaceSheet.CacheArea, CacheKeys.Definition, def, 270);
                return def;
            }        

            public List<Rider> GetRidersOf(string category)
            {
                var riders = GetRiders();
                if (riders.ContainsKey(category))
                {
                    return riders[category];
                }
                return new List<Rider>();
            }

            public Rider GetRider(string category, int number)
            {
                var riders = this.GetRidersOf(category);
                foreach (var rider in riders)
                {
                    if (rider.Number == number)
                    {
                        return rider;
                    }
                }
                throw new Exception("Rider not found");
            }

            public Dictionary<string,List<Rider>> GetRiders()
            {
                if (Cache.Has(RaceSheet.CacheArea, CacheKeys.Riders))
                {
                    if (!Cache.HasExpired(RaceSheet.CacheArea, CacheKeys.Riders))
                    {
                        return (Dictionary<string, List<Rider>>)Cache.Get(RaceSheet.CacheArea, CacheKeys.Riders);
                    }
                    Cache.ExtendExpiration(RaceSheet.CacheArea, CacheKeys.Riders, 20);
                }            
                var def = GetDefinition();
                var cats = GetCategories();
                var riders = new Dictionary<string, List<Rider>>();
                if (cats.Count == 0)
                {
                    Cache.Add(RaceSheet.CacheArea, CacheKeys.Riders, riders, 18);
                    return riders;
                }
                var maxNumberOfSections = Category.GetMaxNumberOfSectionInCategoryList(cats);
                var skipNextRow = false;
                var currentCat = cats[0];
                foreach (var row in GetRiderRange())
                {
                    if (row.Count == 0)
                    {
                        continue;
                    }
                    else if (row.Count == 1)
                    {
                        var t = row[0].ToString();
                        currentCat = cats.Find(c => c.Name == t);
                        riders[currentCat.Name] = new List<Rider>();
                        // Console.WriteLine("Cats length :" + GetCategories().Count);
                        // cats.Remove(currentCat);
                        skipNextRow = true;
                    } else if (skipNextRow)
                    {
                        skipNextRow = false;
                    } else
                    {                    
                        riders[currentCat.Name].Add(Rider.GenerateFromRow(row, def, currentCat, maxNumberOfSections));
                    }

                }
                Cache.Add(RaceSheet.CacheArea, CacheKeys.Riders, riders, 18);
                return riders;
            }

            private IList<IList<Object>> GetRiderRange()
            {                        
                return Reader.GetRange("Results!A3:XX1000");            
            }

            public int SolveSectionForReferee(string code)
            {
                RefereeCodes codes;
                var isInCache = Cache.Has(RaceSheet.CacheArea, CacheKeys.RefereeCodes);
                if (!isInCache ||
                    Cache.HasExpired(RaceSheet.CacheArea, CacheKeys.RefereeCodes))
                {                
                    if (isInCache)
                    {             
                        Cache.ExtendExpiration(RaceSheet.CacheArea, CacheKeys.RefereeCodes, 20);
                    }
                    var matrix = RefereeReader.GetRange("Codes!A2:B20");
                    codes = RefereeCodes.GenerateFromMatrix(matrix);
                    Cache.Add(RaceSheet.CacheArea, CacheKeys.RefereeCodes, codes, 3600);
                } else
                {                
                    codes = (RefereeCodes)Cache.Get(RaceSheet.CacheArea, CacheKeys.RefereeCodes);
                }
                if (!codes.HasCode(code))
                {
                    throw new Exception("Code does not exist");
                }
                return codes.GetSectionForCode(code);                            
            }

            private int GetSheetId(string sheet, IGoogleSheetReader reader)
            {
                Dictionary<string, int> data = new Dictionary<string, int>();
                int sheetId = 0;
                string key = reader.GetSpreadsheetId() + "_" + sheet;
                Boolean hasExpired = true;
                Boolean has = Cache.Has(RaceSheet.CacheArea, CacheKeys.SheetIds);
                if (has)
                {        
                    data = (Dictionary<string, int>)Cache.Get(RaceSheet.CacheArea, CacheKeys.SheetIds);
                    hasExpired = Cache.HasExpired(RaceSheet.CacheArea, CacheKeys.SheetIds);
                    var hasKey = data.ContainsKey(key);
                    if (hasKey && !hasExpired)
                    {
                        return data[key];
                    }
                    else if (!hasExpired)
                    {
                        sheetId = reader.GetSheetId(sheet);
                        data.Add(key, sheetId);
                        Cache.ReplaceValue(RaceSheet.CacheArea, CacheKeys.SheetIds, data);
                    }
                }
                if (hasExpired) {
                    if (has)
                    {
                        Cache.ExtendExpiration(RaceSheet.CacheArea, CacheKeys.SheetIds, 20);
                    }
                    sheetId = reader.GetSheetId(sheet);
                    data = new Dictionary<string, int>()
                    {
                        {key, sheetId}
                    };
                    Cache.Add(RaceSheet.CacheArea, CacheKeys.SheetIds, data, 8 * 3600);
                }            
                return sheetId;
            }

            private CellData CreateCellData(string value)
            {
                var cellData = new CellData();
                ExtendedValue extendedValue;            
                extendedValue = new ExtendedValue { StringValue = value };
                cellData.UserEnteredValue = extendedValue;
                cellData.UserEnteredFormat = GetAlignedFormat("left");
                return cellData;
            }

            private CellData CreateCellData(int value)
            {
                var cellData = new CellData();
                ExtendedValue extendedValue;
                extendedValue = new ExtendedValue { NumberValue = value };
                cellData.UserEnteredValue = extendedValue;
                cellData.UserEnteredFormat = GetAlignedFormat("left");
                return cellData;
            }

            private CellFormat GetAlignedFormat(string align, bool addBorder = false)
            {
                var cellFormat = new CellFormat();
                cellFormat.HorizontalAlignment = align;
                if (addBorder)
                {
                    Border border = new Border
                    {
                        Style = "SOLID",
                        Width = 1,
                        Color = new Google.Apis.Sheets.v4.Data.Color
                        {
                            Red = 0,
                            Green = 0,
                            Blue = 0
                        }
                    };
                    cellFormat.Borders = new Borders
                    {
                        Bottom = border,
                        Top = border,
                        Left = border,
                        Right = border
                    };
                }            
                return cellFormat;
            }    

            private void SolveActionLogRow()
            {
                // We keep this in memory, so there is no need to cache this
                var range = Reader.GetRange("Action log!A1:A10000");
                var index = 0;
                foreach (var row in range)
                {
                    index++;
                    if (row.Count == 0 || row[0].ToString() == "")
                    {
                        break;
                    }                
                }
                ActionLogRow = index;
            }

            private Request GetActionLogCreateRequestsForResult(List<SaveResultDTO> results)
            {
                if (ActionLogRow == -1)
                {
                    SolveActionLogRow();
                }
                var rowIndex = ActionLogRow;
                ActionLogRow += results.Count;
                GridCoordinate gc = new GridCoordinate
                {
                    ColumnIndex = 0,
                    RowIndex = rowIndex,
                    SheetId = GetSheetId("Action log", Reader)
                };
                var request = new Request { UpdateCells = new UpdateCellsRequest { Start = gc, Fields = "*" } };
                var listRowData = new List<RowData>();            
                foreach (var result in results)
                {
                    var rowData = new RowData();
                    var listCellData = new List<CellData>();
                    var now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    listCellData.Add(CreateCellData(now));
                    listCellData.Add(CreateCellData(Util.GetIP()));
                    if (result.RemovePoints)
                    {
                        listCellData.Add(CreateCellData("Clear points"));
                    }
                    else
                    {
                        listCellData.Add(CreateCellData("Set points"));
                    }
                    listCellData.Add(CreateCellData(result.Category));
                    listCellData.Add(CreateCellData(result.RiderName));
                    listCellData.Add(CreateCellData(result.Lap));
                    listCellData.Add(CreateCellData(result.Section));
                    if (!result.RemovePoints)
                    {
                        listCellData.Add(CreateCellData(result.Points));
                        if (result.SectionTime != null)
                        {
                            listCellData.Add(CreateCellData(result.SectionTime));
                        }
                    }
                    rowData.Values = listCellData;
                    listRowData.Add(rowData);                        
                }
                request.UpdateCells.Rows = listRowData;            
                return request;
            }

            void IRaceSheet.SaveResult(List<SaveResultDTO> results)
            {
                // https://www.hardworkingnerd.com/how-to-read-and-write-to-google-sheets-with-c/
                var requests = new BatchUpdateSpreadsheetRequest { Requests = new List<Request>() };
                var sheetId = GetSheetId("Results", Reader);
                // This can't be cached because data can be sorted in sheets ui anytime and row numbers change
                var range = Reader.GetRange("Results!A1:B1000");

                foreach (var result in results)
                {
                    var section = SolveSectionForReferee(result.Code);
                    if (section != result.Section)
                    {
                        throw new Exception("Referee code has changed. Can't save result.");
                    }
                    // result
                    GridCoordinate gc = new GridCoordinate
                    {
                        ColumnIndex = SolveColumnForResult(result.Lap, result.Section),
                        RowIndex = SolveRowForRider(result.Category, result.RiderNumber, range),
                        SheetId = sheetId
                    };
                    var request = new Request { UpdateCells = new UpdateCellsRequest { Start = gc, Fields = "*" } };
                    var listRowData = new List<RowData>();
                    var listCellData = new List<CellData>();
                                    
                    var cellData = new CellData();
                    ExtendedValue extendedValue;
                    if (result.RemovePoints)
                    {
                        extendedValue = new ExtendedValue { StringValue = "" };
                    }
                    else
                    {
                        extendedValue = new ExtendedValue { NumberValue = result.Points };
                    }                
                    cellData.UserEnteredFormat = GetAlignedFormat("center", true);
                    cellData.UserEnteredValue = extendedValue;
                    listCellData.Add(cellData);
                    var rowData = new RowData();
                    rowData.Values = listCellData;
                    listRowData.Add(rowData);                
                    request.UpdateCells.Rows = listRowData;
                    requests.Requests.Add(request);                                                
                }
                requests.Requests.Add(GetActionLogCreateRequestsForResult(results));            
                Reader.Update(requests);
                // Update cache
                UpdateRidersCacheFromResults(results);
            }

            private void UpdateRidersCacheFromResults(List<SaveResultDTO> results)
            {
                if (!Cache.Has(RaceSheet.CacheArea, CacheKeys.Riders) ||
                    Cache.HasExpired(RaceSheet.CacheArea, CacheKeys.Riders))
                {
                    // No need to update, because cache has expired
                    return;
                }
                var riders = GetRiders();
                foreach (var result in results)
                {
                    if (riders.ContainsKey(result.Category))
                    {
                        foreach (var rider in riders[result.Category])
                        {
                            if (rider.Number == result.RiderNumber)
                            {
                                if (!rider.SectionResults.ContainsKey(result.Lap))
                                {
                                    rider.SectionResults.Add(result.Lap, new Dictionary<int, int>());
                                }                            
                                if (!rider.SectionResults[result.Lap].ContainsKey(result.Section))
                                {
                                    rider.SectionResults[result.Lap][result.Section] = 0;
                                }
                                if (result.RemovePoints)
                                {
                                    rider.SectionResults[result.Lap].Remove(result.Section);
                                } else
                                {
                                    rider.SectionResults[result.Lap][result.Section] = result.Points;
                                }
                                break;
                            }
                        }
                    }
                }
                // Then save to cache, but don't reset time because there might be other changes in sheet ui
                if (Cache.Has(RaceSheet.CacheArea, CacheKeys.Riders))
                {
                    Cache.ReplaceValue(RaceSheet.CacheArea, CacheKeys.Riders, riders);
                }
            }

            private int SolveColumnForResult(int lap, int section)
            {
                var def = GetDefinition();
                var sections = Category.GetMaxNumberOfSectionInCategoryList(GetCategories());
                return 1 + def.RiderColumnNames.Count + section + (sections + 1) * (lap - 1);
            }

            private int SolveRowForRider(string category, int riderNumber, IList<IList<object>> range)
            {                
                bool categoryFound = false;
                var index = 0;
                foreach (var row in range)
                {
                    if (row.Count == 0)
                    {
                        index++;
                        continue;
                    }
                    if (categoryFound && riderNumber.ToString() == row[1].ToString())
                    {
                        return index;
                    }
                    else if (category == row[0].ToString())
                    {
                        categoryFound = true;
                    }
                    index++;
                }
                throw new Exception("Row not found");
            }

            public List<QueueDTO> RemoveUntakenRiders()
            {
                return GetQueueAndClearEmptyIfNeeded(true).GetFullData();
            }

            private Queue GetQueue()
            {
                return GetQueueAndClearEmptyIfNeeded(false);
            }

            public void ReleaseQueuedRider(string code)
            {
                var queue = GetQueue();
                if (queue.IsQueueing(code))
                {
                    throw new AppException(
                        "Rider is in queue and can't be released",
                        Error.RIDER_RELEASE_IN_QUEUE,
                        System.Net.HttpStatusCode.Conflict);                
                }
                queue.ReleaseRiderByCode(code);
                CacheQueue(queue);
            }

            public void RemoveRiderFromQueueByReferee(RefereeRemoveRiderFromQueueDTO data)
            {
                var queue = GetQueue();
                var section = SolveSectionForReferee(data.Code);
                foreach (KeyValuePair<string, QueueDTO> kvp in queue.CodeToRider) {
                    if (kvp.Value.Category == data.Category &&
                        kvp.Value.RiderName == data.RiderName &&
                        kvp.Value.RiderNumber == data.RiderNumber)
                    {
                        if (!queue.CodeToSection.ContainsKey(kvp.Key) ||
                            section != queue.CodeToSection[kvp.Key].Section)
                        {
                            throw new Exception("Rider not in queue anymore");
                        } else
                        {
                            queue.ReleaseSectionForCode(kvp.Key);
                        }
                    }
                }                         
            }

            private Queue GetQueueAndClearEmptyIfNeeded(bool clearEmpty)
            {
                Queue queue;
                var isInCache = Cache.Has(RaceSheet.CacheArea, CacheKeys.Queue);
                if (clearEmpty ||
                    !isInCache ||
                    Cache.HasExpired(RaceSheet.CacheArea, CacheKeys.Queue))
                {                
                    if (isInCache && !clearEmpty)
                    {
                        UploadQueue();
                        Cache.ExtendExpiration(RaceSheet.CacheArea, CacheKeys.Queue, 30);
                    }
                    var matrix = RefereeReader.GetRange("Queuing!A2:F200");
                    queue = Queue.GenerateFromMatrix(matrix);
                    if (!isInCache || !clearEmpty)
                    {
                        Cache.Add(RaceSheet.CacheArea, CacheKeys.Queue, queue, 8 * 3600);
                    }
                    else if (clearEmpty)
                    {
                        // is in cache
                        var cachedQueue = (Queue)Cache.Get(RaceSheet.CacheArea, CacheKeys.Queue);
                        cachedQueue.RemoveIfNotIn(queue);
                        Cache.ReplaceValue(RaceSheet.CacheArea, CacheKeys.Queue, cachedQueue);
                        return cachedQueue;
                    }
                }
                else
                {                
                    queue = (Queue)Cache.Get(RaceSheet.CacheArea, CacheKeys.Queue);             
                }
                return queue;
            }

            private void CacheQueue(Queue queue)
            {            
                Cache.ReplaceValue(RaceSheet.CacheArea, CacheKeys.Queue, queue);
            }

            public string TakeRiderQueueToControl(QueueDTO rider)
            {
                var queue = GetQueue();
                if (!queue.IsRiderAvailable(rider)) {
                    throw new AppException(
                        "Rider is not available",
                        Error.RIDER_PICK_NOT_AVAILABLE,
                        System.Net.HttpStatusCode.Conflict);                
                }
                var code = queue.PickRider(rider);          
                if (queue.ShouldUpdate(100))
                {
                    UploadQueue(queue);
                }
                CacheQueue(queue);
                return code;
            }

            public List<QueueDTO> GetQueueData()
            {
                var queue = GetQueue();
                return queue.GetFullData();
            }

            public void UploadQueue()
            {
                var queue = GetQueue();            
                UploadQueue(queue);
            }

            private void UploadQueue(Queue queue)
            {
                GridCoordinate gc = new GridCoordinate
                {
                    ColumnIndex = 0,
                    RowIndex = 1,
                    SheetId = GetSheetId("Queuing", RefereeReader)
                };
                var requests = new BatchUpdateSpreadsheetRequest { Requests = new List<Request>() };
                var request = new Request { UpdateCells = new UpdateCellsRequest { Start = gc, Fields = "*" } };
                var listRowData = new List<RowData>();            
                foreach (var q in queue.GetFullData())
                {
                    var rowData = new RowData();
                    var listCellData = new List<CellData>();
                    listCellData.Add(CreateCellData(q.Category));
                    listCellData.Add(CreateCellData(q.RiderNumber));
                    listCellData.Add(CreateCellData(q.RiderName));
                    listCellData.Add(CreateCellData(q.Code));
                    if (q.Section != 0)
                    {
                        listCellData.Add(CreateCellData(q.Section));
                        var time = q.Arrived.ToString(Queue.DISPLAY_DATE);
                        listCellData.Add(CreateCellData(time));
                    } else
                    {
                        listCellData.Add(CreateCellData(""));
                        listCellData.Add(CreateCellData(""));
                    }
                    rowData.Values = listCellData;
                    listRowData.Add(rowData);                
                }
                // Pad empty to overwrite data
                var emptyCell = CreateCellData("");
                var emptyRow = new RowData();
                emptyRow.Values = new List<CellData>{
                    emptyCell,emptyCell,emptyCell,
                    emptyCell,emptyCell,emptyCell
                };            
                while (listRowData.Count < 100)
                {                
                    listRowData.Add(emptyRow);
                }
                if (listRowData.Count > 0)
                {
                    request.UpdateCells.Rows = listRowData;
                    requests.Requests.Add(request);
                    RefereeReader.Update(requests);
                }
            }

            public QueueStatusAndResultDTO GetResultAndQueueStatusForCode(string code)
            {
                var queue = GetQueue();
                var queueRider = queue.CodeToRider[code];
                var rider = GetRider(queueRider.Category, queueRider.RiderNumber);
                // refactor this away from here
                var returnRider = new QueueStatusAndResultDTO
                {
                    RiderData = rider.RiderData,
                    Position = rider.Position,
                    Number = rider.Number,
                    SectionResults = rider.SectionResults,
                    Penalties = rider.Penalties,
                    Notes = rider.Notes,
                    Time = rider.Time
                };
                if (queue.CodeToSection.ContainsKey(code))
                {
                    returnRider.QueueInSection = queue.CodeToSection[code].Section;
                    returnRider.AheadInQueue = queue.SolveHowManyAreInFrontOfCode(code);

                } else
                {
                    returnRider.QueueInSection = 0;
                    returnRider.AheadInQueue = 0;
                }
                return returnRider;
            }

            public bool QueueToSectionByCode(string code, int section)
            {
                var queue = GetQueue();
                queue.SetSectionForCode(code, section);
                CacheQueue(queue);
                return true;
            }

            public bool RemoveFromQueueByCode(string code)
            {
                var queue = GetQueue();
                queue.ReleaseSectionForCode(code);
                CacheQueue(queue);
                return true;
            }

            public Dictionary<int,int> GetQueueLengths()
            {
                return GetQueue().GetQueueLengths();
            }

            public IList<SafeQueueDTO> GetQueueOfSection(int section)
            {
                return GetQueue().GetQueueOfSection(section);
            }

        }
    }
