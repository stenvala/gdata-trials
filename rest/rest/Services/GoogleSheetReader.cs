﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace TrialApp.Services
{

    public interface IGoogleSheetReaderFactory
    {
        IGoogleSheetReader Create(string spreadsheetId);
    }

    public class GoogleSheetReaderFactory : IGoogleSheetReaderFactory
    {        

        public IGoogleSheetReader Create(string spreadsheetId)
        {
            return new GoogleSheetReader(spreadsheetId);
        }
    }

    public interface IGoogleSheetReader
    {        
        IList<IList<Object>> GetRange(String range);
        GridData GetGridDataForRange(String range);
        int GetSheetId(string spreadSheetName);
        string GetSpreadsheetId();
        BatchUpdateSpreadsheetResponse Update(BatchUpdateSpreadsheetRequest request);
    }    

    public class GoogleSheetReader : IGoogleSheetReader
    {

        static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static readonly string ApplicationName = "Google Sheets Reader";

        private readonly SheetsService Service;
        private readonly string SpreadsheetId;

        public GoogleSheetReader(string spreadsheetId)
        {
            SpreadsheetId = spreadsheetId;
            UserCredential credential;

            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }
            // Create Google Sheets API service.
            Service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
        }

        public string GetSpreadsheetId()
        {
            return SpreadsheetId;
        }
        // Returns just matrix of values (fast)
        public IList<IList<Object>> GetRange(String range)
        {
            SpreadsheetsResource.ValuesResource.GetRequest request =
               Service.Spreadsheets.Values.Get(SpreadsheetId, range);
            ValueRange response = request.Execute();            
            return response.Values;
        }
        // Returns all data for range, including all cell formats (slow)
        public GridData GetGridDataForRange(String range)
        {
            var request =
               Service.Spreadsheets.Get(SpreadsheetId);
            request.IncludeGridData = true;
            request.Ranges = new List<string>() { range };
            var response = request.Execute();
            var sheet = response.Sheets[0];
            return sheet.Data[0];            
        }

        public int GetSheetId(string spreadSheetName)
        {
            var spreadsheet = Service.Spreadsheets.Get(SpreadsheetId).Execute();
            var sheet = spreadsheet.Sheets.FirstOrDefault(s => s.Properties.Title == spreadSheetName);
            int sheetId = (int)sheet.Properties.SheetId;
            return sheetId;
        }

        public BatchUpdateSpreadsheetResponse Update(BatchUpdateSpreadsheetRequest request)
        {                        
            return Service.Spreadsheets.BatchUpdate(request, SpreadsheetId).Execute();
        }
    }
}
