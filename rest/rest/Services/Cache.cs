﻿using System;
using System.Collections.Generic;

namespace TrialApp.Services
{
    public interface ICache
    {
        void Clear();
        void Clear(string area);
        void Clear(string area, string key);
        void Clear(string area, List<string> keys);
        ICache Add(string area, string key, Object data);
        ICache Add(string area, string key, Object data, int expiresInSeconds);
        ICache Add(string area, string key, Object data, DateTime expires);
        void ReplaceValue(string area, string key, Object value);
        Object Get(string area, string key);
        Boolean HasExpired(string area, string key);
        void ExtendExpiration(string area, string key, int seconds);
        Boolean Has(string area, string key);
    }

    public class Cache : ICache
    {
        private readonly Dictionary<string, Dictionary<string, (DateTime, Object)>>Data;

        public Cache()
        {
            Data = new Dictionary<string, Dictionary<string, (DateTime, Object)>>();
        }

        public void Clear()
        {
            Data.Clear();
        }

        public void Clear(string area)
        {
            if (Data.ContainsKey(area))
            {
                Data.Remove(area);
            }            
        }

        public void Clear(string area, string key)
        {
            if (Data.ContainsKey(area) && Data[area].ContainsKey(key))
            {
                Data[area].Remove(key);
                if (Data[area].Count == 0)
                {
                    Data.Remove(area);
                }
            }
        }

        public void Clear(string area, List<string> keys)
        {
            keys.ForEach(key =>
                Clear(area, key)
            );
        }

        public ICache Add(string area, string key, Object data)
        {
            var expires = DateTime.Now;            
            return Add(area, key, data, expires.AddSeconds(120));
        }

        public ICache Add(string area, string key, Object data, int expiresInSeconds)
        {
            var expires = DateTime.Now;
            return Add(area, key, data, expires.AddSeconds(expiresInSeconds));
        }
        
        public ICache Add (string area, string key, Object data, DateTime expires)
        {
            var entry = (expires, data);
            if (!Data.ContainsKey(area))
            {
                Data.Add(area, new Dictionary<string, (DateTime, object)>());                  
            }
            if (Data[area].ContainsKey(key))
            {
                Data[area][key] = entry;
            }
            else {
                Data[area].Add(key, entry);
            }
            return this;
        }

        public void ReplaceValue(string area, string key, Object value)
        {
            var newEntry = (Data[area][key].Item1, value);
            Data[area][key] = newEntry;
        }

        public Object Get(string area, string key)
        {
            if (!Data.ContainsKey(area) || !Data[area].ContainsKey(key))
            {
                throw new Exception("Key " + key + " doesn't exist in cache");
            }
            return Data[area][key].Item2;
        }

        public Boolean HasExpired(string area, string key)
        {
            return Data[area][key].Item1 < DateTime.Now;
        }

        public void ExtendExpiration(string area, string key, int seconds)
        {
            var expires = DateTime.Now;
            expires.AddSeconds(seconds);
            var newEntry = (expires, Data[area][key].Item2);
            Data[area][key] = newEntry;           
        }

        public Boolean Has(string area, string key)
        {
            return Data.ContainsKey(area) && Data[area].ContainsKey(key);
        }
    }
}
