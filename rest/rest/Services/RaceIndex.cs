﻿using System;
using System.Collections.Generic;
using TrialApp.Models;

namespace TrialApp.Services
{
    public interface IRaceIndex
    {
        List<Race> Get();
        bool CanRefereeRace(string raceId);
        void ClearCache();
    }

    public class RaceIndex : IRaceIndex
    {        
        private readonly ICache Cache;        
        private readonly IGoogleSheetReader Reader;
        static private readonly string SheetId = "1z91HyWqttWf-uL5V1gbsjC1-tuasC-CVzqOjmTxwBsk";
        static private readonly string CacheArea = "RaceIndex";

        public RaceIndex(ICache cache, IGoogleSheetReaderFactory googleSheetReaderFactory)           
        {
            Cache = cache;
            Reader = googleSheetReaderFactory.Create(RaceIndex.SheetId);
        }

        public void ClearCache()
        {
            Cache.Clear(RaceIndex.CacheArea);
        }

        public bool CanRefereeRace(string raceId)
        {
            foreach(var race in Get())
            {
                if (race.Id == raceId)
                {
                    return race.Refereeing;
                }
            }
            return false;
        }

        public List<Race> Get()
        {            
            if (Cache.Has(RaceIndex.CacheArea, SheetId))
            {                
                if (!Cache.HasExpired(RaceIndex.CacheArea, SheetId))
                {
                    var c = (List<Race>)Cache.Get(RaceIndex.CacheArea, SheetId);
                    return c;
                }             
                Cache.ExtendExpiration(RaceIndex.CacheArea, SheetId, 20);
            }            
            IList<IList<Object>> values = Reader.GetRange("Index!A2:G100");
            var races = new List<Race>();
            foreach (var row in values)
            {
                if (row.Count == 0)
                {
                    break;
                }
                if (row[5].ToString() != "1")
                {
                    races.Add(Race.GenerateFromRow(row));
                }

            }
            Cache.Add(RaceIndex.CacheArea, SheetId, races, 3600 * 24);
            return races;
        }        
    }
}
