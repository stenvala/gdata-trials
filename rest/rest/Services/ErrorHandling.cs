﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TrialApp.Services
{

    
    public enum Error {
        RIDER_RELEASE_IN_QUEUE,        
        RIDER_PICK_NOT_AVAILABLE,
        UNDEFINED
    }
    
    public class ExceptionDTO
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Error kind;
        public string message;
        public object data;
    }

    public class AppException : Exception
    {        
        public Error Kind;
        public HttpStatusCode StatusCode;        

        public AppException(string msg, Error kind, HttpStatusCode code) : base(msg)
        {            
            Kind = kind;
            StatusCode = code;
        }
       

    }    

    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var statusCode = HttpStatusCode.InternalServerError;
            var response = new ExceptionDTO
            {
                message = ex.Message,
                kind = Error.UNDEFINED
            };
            if (ex.Data != null)
            {
                response.data = ex.Data;
            }
            if (ex is AppException)
            {                
                response.kind = ((AppException)ex).Kind;
                statusCode = ((AppException)ex).StatusCode;
            }

            var result = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) statusCode;
            return context.Response.WriteAsync(result);
        }
    }
}
