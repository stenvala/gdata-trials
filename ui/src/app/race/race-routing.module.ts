import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { IndexComponent } from "./components/index/index.component";
import { SectionCardsComponent } from "./components/section-cards/section-cards.component";
import { ResultsComponent } from "./components/results/results.component";
import { RefereeingComponent } from "./components/refereeing/refereeing.component";
import { QueuingComponent } from "./components/queuing/queuing.component";

const routes: Routes = [
  { path: "index", component: IndexComponent },
  { path: ":sheetId/section-cards", component: SectionCardsComponent },
  { path: ":sheetId/results", component: ResultsComponent },
  { path: ":sheetId/refereeing", component: RefereeingComponent },
  { path: ":sheetId/queuing", component: QueuingComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RaceRoutingModule {}
