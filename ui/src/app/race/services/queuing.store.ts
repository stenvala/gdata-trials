import { Injectable } from "@angular/core";
import { observable } from "mobx-angular";
import { ControlStateService } from "@core/services/control-state.service";
import { QueueRider } from "./types";

@Injectable({
  providedIn: "root"
})
export class QueuingStore {
  static LS_KEY = "QUEUING_STORE_MY_RIDERS";
  static LS_VALID_KEY = "QUEUING_STORE_MY_RIDERS_DATE";

  @observable
  myRiders: QueueRider[] = [];

  constructor(public controlStateService: ControlStateService) {
    const validUntil = this.controlStateService.get<number>(
      QueuingStore.LS_VALID_KEY,
      0
    );
    this.myRiders =
      validUntil > new Date().getTime()
        ? this.controlStateService.get<QueueRider[]>(QueuingStore.LS_KEY, [])
        : [];
  }

  public add(category: string, number: number, name: string, code: string) {
    if (this.myRiders.length === 0) {
      this.controlStateService.set(
        QueuingStore.LS_VALID_KEY,
        new Date().getTime() + 8 * 3600 * 1000
      );
    }
    this.myRiders = this.myRiders.concat([
      {
        category: category,
        riderNumber: number,
        riderName: name,
        code: code
      }
    ]);
    this.controlStateService.set(QueuingStore.LS_KEY, this.myRiders);
  }

  public remove(code: string) {
    this.myRiders = this.myRiders.filter(rider => rider.code !== code);
    if (this.myRiders.length === 0) {
      this.controlStateService.remove(QueuingStore.LS_KEY);
      this.controlStateService.remove(QueuingStore.LS_VALID_KEY);
    } else {
      this.controlStateService.set(QueuingStore.LS_KEY, this.myRiders);
    }
  }
}
