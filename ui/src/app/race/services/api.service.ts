import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import {
  Definition,
  Category,
  Rider,
  Race,
  CategoryToRiders,
  SavePointsDTO,
  TakeRiderDTO,
  TakeRiderReplyDTO,
  QueuingRider,
  SectionQueuer,
  RemoveFromQueueByRefereeDTO
} from "./types";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  private baseUrl: string = "/api/race";

  constructor(private http: HttpClient) {}

  fetchDefinition(raceId: String): Observable<Definition> {
    return this.http.get<Definition>(
      this.baseUrl + "/" + raceId + "/definition"
    );
  }

  fetchCategories(raceId: String): Observable<Category[]> {
    return this.http.get<Category[]>(
      this.baseUrl + "/" + raceId + "/categories"
    );
  }

  fetchResults(raceId: String): Observable<CategoryToRiders> {
    return this.http.get<CategoryToRiders>(
      this.baseUrl + "/" + raceId + "/riders"
    );
  }

  fetchIndex(): Observable<Race[]> {
    return this.http.get<Race[]>(this.baseUrl + "/index");
  }

  clearRaceCache(raceId: string): any {
    return this.http.get<Race[]>(this.baseUrl + "/" + raceId + "/clear");
  }

  getSectionNumberForCode(raceId: string, code: string): Observable<number> {
    return this.http
      .post<any>(this.baseUrl + "/" + raceId + "/refereeing/solve-section", {
        code: code
      })
      .pipe(map(reply => reply.section));
  }

  savePoints(raceId: string, data: SavePointsDTO[]) {
    return this.http.post<any>(
      this.baseUrl + "/" + raceId + "/refereeing/save-result",
      data
    );
  }

  takeRider(raceId: string, data: TakeRiderDTO) {
    return this.http.post<TakeRiderReplyDTO>(
      this.baseUrl + "/" + raceId + "/queuing",
      data
    );
  }

  releaseRider(raceId: string, code: string) {
    return this.http.delete<any>(
      this.baseUrl + "/" + raceId + "/queuing/" + code
    );
  }

  queueRiderInSection(raceId: string, code: string, section: number) {
    return this.http.put<any>(
      this.baseUrl + "/" + raceId + "/queuing/" + code + "/section/" + section,
      {}
    );
  }

  removeRiderFromQueue(raceId: string, code: string) {
    return this.http.delete<any>(
      this.baseUrl + "/" + raceId + "/queuing/" + code + "/section/"
    );
  }

  removeRiderFromSectionQueueByReferee(
    raceId: string,
    data: RemoveFromQueueByRefereeDTO
  ) {
    return this.http.put<any>(
      this.baseUrl + "/" + raceId + "/queuing/remove-by-referee",
      data
    );
  }

  queueLengths(raceId: string) {
    return this.http.get<any>(
      this.baseUrl + "/" + raceId + "/queuing/queue-lengths/"
    );
  }

  queueOfSection(raceId: string, section: number) {
    return this.http.get<SectionQueuer[]>(
      this.baseUrl + "/" + raceId + "/queuing/of-section/" + section
    );
  }

  getRiderResultsByCode(raceId: string, code: string) {
    return this.http.get<QueuingRider>(
      this.baseUrl + "/" + raceId + "/queuing/" + code + "/result"
    );
  }

  saveSelectedRiderData(raceId: string) {
    return this.http.get<any>(this.baseUrl + "/" + raceId + "/queuing/save");
  }

  downsyncSelectedRiderData(raceId: string) {
    return this.http.get<any>(
      this.baseUrl + "/" + raceId + "/queuing/reset-empty"
    );
  }
}
