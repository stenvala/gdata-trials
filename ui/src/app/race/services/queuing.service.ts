import { Injectable } from "@angular/core";
import { ApiService } from "@race/services/api.service";
import { RaceService } from "@race/services/race.service";
import { tap } from "rxjs/operators";
import { Observable } from "rxjs";
import { TakeRiderReplyDTO, QueueRider } from "./types";
import { QueuingStore } from "./queuing.store";
import { HttpErrorResponse } from "@angular/common/http";
import { ErrorDTO } from "@core/error-type";
import { ToasterService } from "@shared/services/toaster.service";

@Injectable({
  providedIn: "root"
})
export class QueuingService {
  constructor(
    private api: ApiService,
    private raceService: RaceService,
    private queuingStore: QueuingStore,
    private toasterService: ToasterService
  ) {}

  takeRider(
    category: string,
    number: number,
    name: string
  ): Observable<TakeRiderReplyDTO> {
    return this.api
      .takeRider(this.raceService.raceId, {
        category: category,
        riderNumber: number,
        riderName: name
      })
      .pipe(
        tap((result: TakeRiderReplyDTO) => {
          this.queuingStore.add(category, number, name, result.code);
        })
      );
  }

  fetchQueueLengths() {
    return this.api.queueLengths(this.raceService.raceId);
  }

  fetchQueueOfSection(section: number) {
    return this.api.queueOfSection(this.raceService.raceId, section);
  }

  queueToSection(rider: QueueRider, section: number) {
    return this.api.queueRiderInSection(
      this.raceService.raceId,
      rider.code,
      section
    );
  }

  removeRiderFromQueue(rider: QueueRider) {
    return this.api.removeRiderFromQueue(this.raceService.raceId, rider.code);
  }

  releaseRider(rider: QueueRider) {
    return this.api.releaseRider(this.raceService.raceId, rider.code).pipe(
      tap(
        _ => this.queuingStore.remove(rider.code),
        (error: HttpErrorResponse) => {
          if ((error.error as ErrorDTO).kind === "RIDER_RELEASE_IN_QUEUE") {
            this.toasterService.showErrorMessage(
              "Kuski on jonossa jaksolle. Sitä ei voida vapauttaa. Lopeta ensin jonotus."
            );
          } else {
            this.queuingStore.remove(rider.code);
          }
        }
      )
    );
  }

  get myRiders() {
    return this.queuingStore.myRiders;
  }
}
