import { Injectable } from "@angular/core";
import { observable, action } from "mobx-angular";
import { Definition, Category, Race, CategoryToRiders } from "./types";

@Injectable({
  providedIn: "root"
})
export class RaceStore {
  @observable
  races: Race[];

  @observable
  raceId: string;

  @observable
  definition: Definition;

  @observable
  categories: Category[] = [];

  @observable
  results: CategoryToRiders = {};

  constructor() {}

  @action
  public resetRace() {
    this.definition = undefined;
    this.categories = [];
    this.results = undefined;
  }
}
