export interface Definition {
  title: string;
  date: string;
  location: string;
  organizer: string;
  translations: Map<string, string>;
  riderColumnNames: string[];
  sectionCardRowHeightScale: number;
}

export type PointSystem = "POINTS" | "PENALTIES";

export interface Category {
  name: string;
  sections: number[];
  laps: number;
  firstNumber: number;
  numberOfRiders: number;
  pointSystem: PointSystem;
  extraCards?: number;
  color: {
    R: number;
    G: number;
    B: number;
  };
}

export interface Rider {
  riderData: Map<string, string>;
  position: number;
  number: number;
  sectionResults: Map<number, Map<number, number>>;
  penalties?: string;
  time?: string;
}

export interface RiderFullResult {
  name: string;
  number: number;
  position: number;
  sectionResults: Map<number, Map<number, number>>;
  points: number;
  totalSections: number;
  combinedResults: { key: number; count: number }[];
}

export interface QueuingRider extends Rider {
  queueInSection: number;
  aheadInQueue: number;
}

export interface SectionQueuer {
  arrived: string;
  category: string;
  riderName: string;
  riderNumber: number;
  section: number;
}

export interface RemoveFromQueueByRefereeDTO {
  code: string;
  category: string;
  riderName: string;
  riderNumber: number;
}

export interface SimpleRider {
  category: string;
  number: number;
  name: string;
  color: {
    R: number;
    G: number;
    B: number;
  };
}

export interface Race {
  date: string;
  name: string;
  id: string;
  refereeing: boolean;
  sectionCards: boolean;
  queuing: boolean;
}

export interface CategoryToRiders {
  [key: string]: Rider[];
}

export interface SavePointsDTO {
  category: string;
  riderNumber: number;
  riderName: string;
  lap: number;
  section: number;
  points?: number;
  removePoints?: boolean;
  code: string;
  sectionTime?: string;
}

export interface EarlierSection {
  section: number;
  code: string;
  expires?: number;
}

export interface TakeRiderDTO {
  category: string;
  riderNumber: number;
  riderName: string;
}

export interface TakeRiderReplyDTO {
  code: string;
}

export interface QueueRider extends TakeRiderDTO, TakeRiderReplyDTO {}
