import { Injectable } from "@angular/core";
import { RaceStore } from "./race.store";
import { observable, action, computed } from "mobx-angular";
import { createTransformer } from "mobx-utils";
import { ApiService } from "./api.service";
import {
  Category,
  Race,
  Rider,
  SimpleRider,
  RiderFullResult,
  PointSystem
} from "./types";
import { tap } from "rxjs/operators";
import { runInAction } from "mobx";
import { forkJoin } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class RaceService {
  @observable
  loaded: boolean = false;
  private loading: boolean = false;

  constructor(private raceStore: RaceStore, private api: ApiService) {}

  getRaces(): Promise<Race[]> {
    if (this.raceStore.races) {
      return Promise.resolve(this.raceStore.races);
    }
    return this.api
      .fetchIndex()
      .pipe(
        tap(races => {
          runInAction(() => (this.raceStore.races = races));
        })
      )
      .toPromise();
  }

  @action
  setRaceId(raceId: string, resetCache: boolean = false): void {
    if (this.raceStore.raceId === raceId) {
      if (resetCache) {
        this.refreshAll(resetCache);
      }
      return;
    }
    this.raceStore.raceId = raceId;
    this.refreshAll(resetCache);
  }

  async refreshAll(resetCache: boolean = false) {
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.loaded = false;
    if (resetCache) {
      await this.api.clearRaceCache(this.raceStore.raceId).subscribe();
    }
    forkJoin({
      definition: this.api.fetchDefinition(this.raceStore.raceId),
      categories: this.api.fetchCategories(this.raceStore.raceId),
      results: this.api.fetchResults(this.raceStore.raceId)
    }).subscribe(
      result => {
        this.raceStore.definition = result.definition;
        this.raceStore.categories = result.categories;
        this.raceStore.results = result.results;
        this.loaded = true;
        this.loading = false;
      },
      () => {
        console.log("fork join failed");
      }
    );
  }

  get raceId() {
    return this.raceStore.raceId;
  }

  get definition() {
    return this.raceStore.definition;
  }

  get categories() {
    return this.raceStore.categories;
  }

  @computed
  get allResults() {
    return this.raceStore.results;
  }

  @computed
  get simpleRiderList(): SimpleRider[] {
    let riders: SimpleRider[] = [];
    for (const ind in this.allResults) {
      const category = this.getCategory(ind);
      riders = riders.concat(
        this.allResults[ind]
          .map((rider: Rider) => {
            return {
              category: category.name,
              color: category.color,
              name: this.getRiderName(rider),
              number: rider.number
            };
          })
          .sort((a, b) => a.number - b.number)
      );
    }
    return riders;
  }

  get nameField() {
    return this.raceStore.definition.riderColumnNames[0];
  }

  getRiderName = createTransformer((rider: Rider): string => {
    return rider.riderData[this.nameField];
  });

  getResultsOf = createTransformer((categoryName: string): Rider[] => {
    if (!(categoryName in this.raceStore.results)) {
      return [];
    }
    return this.raceStore.results[categoryName];
  });

  getRiderFullResultOf(
    categoryName: string,
    riderNumber: number
  ): RiderFullResult {
    return this.getSolvedResultsOf(categoryName).find(
      rider => riderNumber === rider.number
    );
  }

  getSolvedResultsOf = createTransformer(
    (categoryName: string): RiderFullResult[] => {
      if (!(categoryName in this.raceStore.results)) {
        return [];
      }
      const pointSystem = this.getCategory(categoryName).pointSystem;
      return this.raceStore.results[categoryName]
        .map((rider: Rider) => {
          return {
            name: this.getRiderName(rider),
            number: rider.number,
            position: -1,
            sectionResults: rider.sectionResults,
            points: this.solvePointsOf(rider),
            totalSections: this.solveTotalSections(rider),
            combinedResults: this.solveCombinedResult(rider, pointSystem)
          };
        })
        .sort((a, b) => {
          if (a.totalSections !== b.totalSections) {
            return a.totalSections > b.totalSections ? -1 : 1;
          }
          if (a.points !== b.points) {
            return (
              (a.points < b.points ? -1 : 1) *
              (pointSystem === "PENALTIES" ? 1 : -1)
            );
          }
          for (var i = 0; i < a.combinedResults.length; i++) {
            if (a.combinedResults[i].count !== b.combinedResults[i].count) {
              return a.combinedResults[i].count > b.combinedResults[i].count
                ? -1
                : 1;
            }
          }
          return a.number < b.number ? -1 : 1;
        })
        .map((rider, index) => {
          rider.position = index + 1;
          return rider;
        });
    }
  );

  private solveTotalSections(rider: Rider): number {
    let sections = 0;
    for (let i in rider.sectionResults) {
      for (let j in rider.sectionResults[i]) {
        sections++;
      }
    }
    return sections;
  }
  private solvePointsOf(rider: Rider): number {
    let points = 0;
    for (let i in rider.sectionResults) {
      for (let j in rider.sectionResults[i]) {
        points += rider.sectionResults[i][j];
      }
    }
    return points;
  }

  private solveCombinedResult(rider: Rider, pointType: PointSystem) {
    let obj: { key: number; count: number }[] = [];
    if (pointType === "PENALTIES") {
      for (let i = 0; i < 6; i++) {
        obj.push({
          key: i,
          count: 0
        });
      }
    } else {
      for (let i = 60; i > -1; i = i - 10) {
        obj.push({
          key: i,
          count: 0
        });
      }
    }
    for (let i in rider.sectionResults) {
      for (let j in rider.sectionResults[i]) {
        const result = rider.sectionResults[i][j];
        if (pointType === "PENALTIES") {
          obj[result].count++;
        } else if (result > 0) {
          obj[Math.abs(result / 10 - 6)].count++;
        }
      }
    }
    return obj;
  }

  getCategory = createTransformer(
    (name: string): Category => {
      return this.categories.find(c => c.name == name);
    }
  );

  fetchRiderResultsByQueueCode(code: string) {
    return this.api.getRiderResultsByCode(this.raceId, code);
  }

  saveSelectedRiderData(raceId: string) {
    return this.api.saveSelectedRiderData(raceId);
  }

  downsyncSelectedRiderData(raceId: string) {
    return this.api.downsyncSelectedRiderData(raceId);
  }
}
