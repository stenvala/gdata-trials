import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { PointEvent, PointEventType } from '../point-event.refereeing';

@Component({
  selector: "app-stopwatch",
  templateUrl: "./stopwatch.component.html",
  styleUrls: ["./stopwatch.component.scss"]
})
export class StopwatchComponent implements OnInit {
  display: {
    left: string;
    gone: string;
  };
  private offset: number;
  private startedAt: number;
  on: boolean;
  private interval: any;
  private previousTime: string = "0.00";

  @Output() onEvent = new EventEmitter<PointEvent>();

  constructor() {}

  ngOnInit() {    
    this.reset(true);
    this.interval = setInterval(() => this.updateTime(), 100);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  public getPreviousTime() {
    return this.previousTime;
  }

  private updateTime() {
    if (!this.on) {
      return;
    }
    const now = new Date().getTime();
    // gone
    const millis = this.offset + (now - this.startedAt);
    const seconds = Math.floor(millis / 1000);
    let secPart = seconds % 60;
    const minPart = (seconds - secPart) / 60;
    const tens = Math.round((millis - seconds * 1000) / 100);
    if (tens === 10) {
      secPart++;
    }
    this.display.gone =
      minPart +
      "." +
      (secPart < 10 ? "0" : "") +
      secPart;      
    if (this.display.gone !== "0.00") {
      this.previousTime = this.display.gone;
    }
    // left
    const secondsLeft = 120 - seconds;
    let secPartLeft = secondsLeft % 60;
    const minPartLeft = (secondsLeft - secPartLeft) / 60;    
    if (secondsLeft >= 0) {
      this.display.left =
        minPartLeft +
        "." +
        (secPartLeft < 10 ? "0" : "") +
        secPartLeft;
    } else {
      this.display.left = "0";
    }
  }

  start() {
    if (this.on) {
      return;
    }
    if (this.offset === 0) {
      this.previousTime = "0.00";
    }
    this.startedAt = new Date().getTime();
    this.on = true;
    const event = new PointEvent(
      PointEventType.TimeStart
    );
    this.onEvent.emit(event)
  }

  stop() {
    if (!this.on) {
      return;
    }
    let now = new Date().getTime();
    this.offset += now - this.startedAt;
    this.on = false;
    const event = new PointEvent(
      PointEventType.TimeEnd,
      this.offset
    );
    this.onEvent.emit(event)
  }

  reset(init : boolean = false) {
    this.display = {
      left: "2.00",
      gone: "0.00"
    };
    const event = new PointEvent(
      PointEventType.TimeReset,
      this.offset
    );
    if (!init) {
      this.onEvent.emit(event)
    }
    this.offset = 0;
    this.on = false;
  }
}
