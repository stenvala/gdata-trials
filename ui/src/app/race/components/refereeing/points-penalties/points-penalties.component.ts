import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PointEvent, PointEventType } from '../point-event.refereeing';

@Component({
  selector: 'app-points-penalties',
  templateUrl: './points-penalties.component.html',
  styleUrls: ['./points-penalties.component.scss']
})
export class PointsPenaltiesComponent implements OnInit {

  public possiblePoints = [...Array(6).keys()];

  @Input() points: number;  

  @Output() onEvent = new EventEmitter<PointEvent>();

  constructor() { }

  ngOnInit() {

  }

  setPoints(points: number) {
    const event = new PointEvent(
      PointEventType.PenaltyPointsSet,
      points
    );
    this.onEvent.emit(event);
  }

}
