import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from "@angular/core";
import { PointEvent, PointEventType } from "../point-event.refereeing";
import { Subject, Subscription } from 'rxjs';

interface Gate {
  passed: boolean;
  number: number;
}

@Component({
  selector: "app-points-gates",
  templateUrl: "./points-gates.component.html",
  styleUrls: ["./points-gates.component.scss"]
})
export class PointsGatesComponent implements OnInit, OnDestroy {
  public gates: Gate[];

  @Input() points: number;
  @Input() resetGates: Subject<number>;
  @Output() onEvent = new EventEmitter<PointEvent>();

  private subscription: Subscription;

  constructor() {}

  ngOnInit() {
    this.initGates(this.points);    
    this.subscription = this.resetGates.subscribe(points => {          
      this.initGates(points)
    });    
  }

  ngOnDestroy() {    
    this.subscription.unsubscribe();    
  }

  private initGates(points : number) {    
    this.gates = [];
    let totalPoints = 0;
    for (let i = 0; i < 6; i++) {
      let passed = totalPoints < points ? true : false;
      if (passed) {
        totalPoints = totalPoints + 10;
      }
      this.gates.push({
        passed: passed,
        number: i + 1
      });
    }
  }

  toggle(gate: Gate) {
    gate.passed = !gate.passed;
    const event = new PointEvent(
      PointEventType.GatePointSet,
      gate.passed ? 10 : 0,
      "Set score for " + gate.number
    );
    this.onEvent.emit(event);
    this.computePoints();
  }

  private computePoints() {
    const points = this.gates.filter(g => g.passed).length * 10;
    const event = new PointEvent(PointEventType.GatePointsSet, points);
    this.onEvent.emit(event);
  }
}
