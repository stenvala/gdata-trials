import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatButtonModule } from "@angular/material";
import { SelectFromQueueModalComponent } from "./select-from-queue-modal.component";
import { FlexModule } from "@angular/flex-layout";

@NgModule({
  declarations: [SelectFromQueueModalComponent],
  imports: [CommonModule, MatButtonModule, FlexModule],
  exports: [SelectFromQueueModalComponent],
  providers: [],
  entryComponents: [SelectFromQueueModalComponent]
})
export class SelectFromQueueModalModule {}
