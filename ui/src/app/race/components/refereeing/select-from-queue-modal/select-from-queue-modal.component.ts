import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SavePointsDTO } from "@race/services/types";

export interface DialogData {
  queue: SavePointsDTO[];
}

@Component({
  selector: "app-select-from-queue-modal",
  templateUrl: "./select-from-queue-modal.component.html",
  styleUrls: ["./select-from-queue-modal.component.scss"]
})
export class SelectFromQueueModalComponent implements OnInit {
  public loading: boolean = true;
  public queue: SavePointsDTO[] = [];

  constructor(
    public dialogRef: MatDialogRef<SelectFromQueueModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: DialogData
  ) {
    this.queue = data.queue;
  }

  ngOnInit() {}

  close(): void {
    this.dialogRef.close();
  }
}
