import { Injectable } from "@angular/core";
import { ApiService } from "../../services/api.service";
import { Observable } from "rxjs";
import {
  Category,
  Rider,
  SavePointsDTO,
  EarlierSection
} from "../../services/types";
import { observable, computed, action } from "mobx-angular";
import { RaceService } from "../../services/race.service";
import { ControlStateService } from "src/app/core/services/control-state.service";
import { toJS, when, runInAction } from "mobx";
import { MatSnackBar } from "@angular/material";

@Injectable()
export class RefereeingService {
  static LS_KEY = "REFEREEING_SERVICE_RESULTS";
  static SAVE_DELAY = 15 * 1000 - 200;
  static MARGINAL = 200;

  @observable
  selectedCategory: Category;

  @observable
  selectedRider: Rider;

  @observable
  selectedSection: number;

  @observable
  isSaving: boolean = false;

  private selectedSectionCode: string;

  @observable
  pointsToSave: SavePointsDTO[] = [];

  private nextPointSaveAfter: number = 0;

  constructor(
    private api: ApiService,
    private raceService: RaceService,
    private controlStateService: ControlStateService,
    private snackBar: MatSnackBar
  ) {
    this.pointsToSave = this.controlStateService.get<SavePointsDTO[]>(
      RefereeingService.LS_KEY,
      []
    );
    if (this.pointsToSave.length > 0) {
      when(
        () => !!this.raceService.raceId,
        () => {
          this.delayedSave();
        }
      );
    }
  }

  @computed
  get unsavedPoints(): SavePointsDTO[] {
    return this.pointsToSave;
  }

  get secondsToNextSave(): number | string {
    let now = new Date().getTime() - RefereeingService.MARGINAL;
    return Math.round(Math.max(0, this.nextPointSaveAfter - now) / 1000);
  }

  @computed
  get riders(): Rider[] {
    if (!this.selectedCategory) {
      return [];
    }
    return this.raceService.getResultsOf(this.selectedCategory.name);
  }

  @computed
  get categories(): Category[] {
    if (!this.selectedSection || !this.raceService.categories) {
      return [];
    }
    return this.raceService.categories.filter(c => {
      return c.sections.indexOf(this.selectedSection) != -1;
    });
  }

  @action
  setSection(section: number | undefined, code: string) {
    this.selectedSectionCode = code;
    this.selectedSection = section;
  }

  @action
  setCategory(category: Category | undefined) {
    this.selectedCategory = category;
  }

  @action
  setRider(rider: Rider | undefined) {
    this.selectedRider = rider;
  }

  private get earlierSectionsCSSID() {
    return "RefereeingEarlierSections" + this.raceService.raceId;
  }

  getEarlierSections() {
    const now = new Date().getTime();
    const earlierSections = this.controlStateService
      .get<EarlierSection[]>(this.earlierSectionsCSSID, [])
      .filter(s => s.expires > now);
    this.controlStateService.set(this.earlierSectionsCSSID, earlierSections);
    return earlierSections;
  }

  addEarlierSection(section: EarlierSection) {
    let earlierSections = this.getEarlierSections().filter(
      s => s.section !== section.section
    );
    section.expires = new Date().getTime() + 1000 * 8 * 3600;
    earlierSections.splice(0, 0, section);
    this.controlStateService.set(this.earlierSectionsCSSID, earlierSections);
    return earlierSections;
  }

  removeEarlierSection(section: number) {
    let earlierSections = this.getEarlierSections().filter(
      s => s.section !== section
    );
    this.controlStateService.set(this.earlierSectionsCSSID, earlierSections);
    return earlierSections;
  }

  getSectionForCode(code: string): Observable<number> {
    return this.api.getSectionNumberForCode(this.raceService.raceId, code);
  }

  removeFromQueueWithCode(category: string, number: number, name: string) {
    return this.api.removeRiderFromSectionQueueByReferee(
      this.raceService.raceId,
      {
        code: this.selectedSectionCode,
        category: category,
        riderName: name,
        riderNumber: number
      }
    );
  }

  private savePointsApiCall(points: SavePointsDTO[]) {
    this.nextPointSaveAfter =
      new Date().getTime() + RefereeingService.SAVE_DELAY;
    return this.api.savePoints(this.raceService.raceId, points);
  }

  @action
  private addDelayedPointSave(obj: SavePointsDTO, reverse: boolean = false) {
    if (
      reverse &&
      this.pointsToSave.some(o => {
        return (
          o.riderNumber === obj.riderNumber &&
          o.lap === obj.lap &&
          o.section === obj.section
        );
      })
    ) {
      // updated afterwards
      return;
    }
    this.pointsToSave = this.pointsToSave.filter(o => {
      return (
        o.riderNumber !== obj.riderNumber ||
        o.lap !== obj.lap ||
        o.section !== obj.section
      );
    });
    this.pointsToSave.push(obj);
    this.controlStateService.set(
      RefereeingService.LS_KEY,
      toJS(this.pointsToSave)
    );
  }

  private delayedSave() {
    let points = this.pointsToSave;
    this.pointsToSave = [];
    this.controlStateService.remove(RefereeingService.LS_KEY);
    this.nextPointSaveAfter =
      new Date().getTime() + RefereeingService.SAVE_DELAY;
    this.isSaving = true;
    this.savePointsApiCall(points).subscribe(
      () => {
        runInAction(() => (this.isSaving = false));
        this.snackBar.open("Viivästetty tallennus onnistui.", "OK!", {
          duration: 2000
        });
      },
      () => {
        runInAction(() => (this.isSaving = false));
        this.snackBar.open(
          "Viivästetty tallennus epäonnistui. Yritetään pian uudestaan.",
          "OK!",
          {
            duration: 2000
          }
        );
        if (this.pointsToSave.length > 0) {
          points.forEach(p => this.addDelayedPointSave(p, true));
        } else {
          this.pointsToSave = points;
          this.controlStateService.set(
            RefereeingService.LS_KEY,
            toJS(this.pointsToSave)
          );
          this.nextPointSaveAfter =
            new Date().getTime() + RefereeingService.SAVE_DELAY * 2;
          setTimeout(() => {
            this.delayedSave();
          }, RefereeingService.SAVE_DELAY * 2 + RefereeingService.MARGINAL);
        }
      }
    );
  }

  @action
  setPoints(
    category: Category,
    rider: Rider,
    lap: number,
    section: number,
    points?: number,
    sectionTime?: string
  ) {
    // create dto and set local changes
    let obj: SavePointsDTO = {
      category: category.name,
      riderNumber: rider.number,
      riderName: this.raceService.getRiderName(rider),
      lap: lap,
      section: section,
      code: this.selectedSectionCode
    };
    if (points || points === 0) {
      obj.points = points;
      rider.sectionResults[lap][section] = points;
    } else {
      delete rider.sectionResults[lap][section];
      obj.removePoints = true;
    }
    if (sectionTime) {
      obj.sectionTime = sectionTime;
    }
    // delayed save
    if (
      this.nextPointSaveAfter > new Date().getTime() ||
      this.pointsToSave.length > 0
    ) {
      if (this.pointsToSave.length != 0) {
        this.addDelayedPointSave(obj);
      } else {
        let nextSave =
          this.nextPointSaveAfter -
          new Date().getTime() +
          RefereeingService.MARGINAL;
        this.addDelayedPointSave(obj);
        setTimeout(() => {
          this.delayedSave();
        }, nextSave);
      }
      return;
    }
    // save
    this.isSaving = true;
    this.savePointsApiCall([obj]).subscribe(
      () => {
        runInAction(() => (this.isSaving = false));
        this.snackBar.open("Tiedot tallennettu onnistuneesti.", "OK!", {
          duration: 2000
        });
      },
      () => {
        runInAction(() => (this.isSaving = false));
        this.snackBar.open(
          "Tietojen tallentaminen epäonnistui. Yritetään pian uudestaan.",
          "OK!",
          {
            duration: 2000
          }
        );
        if (this.pointsToSave.length != 0) {
          this.addDelayedPointSave(obj, true);
        } else {
          this.addDelayedPointSave(obj);
          setTimeout(() => {
            this.delayedSave();
          }, RefereeingService.SAVE_DELAY + RefereeingService.MARGINAL);
        }
      }
    );
  }
}
