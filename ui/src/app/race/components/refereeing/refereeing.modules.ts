import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatProgressSpinnerModule,
  MatCardModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule
} from "@angular/material";
import { FlexModule } from "@angular/flex-layout";
import { RefereeingComponent } from "./refereeing.component";
import { RefereeingService } from "./refereeing.service";
import { RaceService } from "../../services/race.service";
import { ApiService } from "../../services/api.service";
import { DetailsDisplayerModule } from "../details-displayer/details-displayer.module";
import { PointsPenaltiesComponent } from "./points-penalties/points-penalties.component";
import { PointsGatesComponent } from "./points-gates/points-gates.component";
import { StopwatchComponent } from "./stopwatch/stopwatch.component";
import { SharedModule } from "src/app/shared/shared.module";
import { SelectFromQueueModalModule } from "./select-from-queue-modal/select-from-queue-modal.module";
import { ShowDelayedQueueModalModule } from "./show-delayed-queue-modal/show-delayed-queue-modal.module";

@NgModule({
  declarations: [
    RefereeingComponent,
    PointsPenaltiesComponent,
    PointsGatesComponent,
    StopwatchComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FlexModule,
    DetailsDisplayerModule,
    SharedModule,
    SelectFromQueueModalModule,
    ShowDelayedQueueModalModule
  ],
  providers: [RefereeingService, RaceService, ApiService]
})
export class RefereeingModule {}
