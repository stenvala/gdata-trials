export enum PointEventType {
  PenaltyPointsSet = 'Penalty points set',
  GatePointsSet = 'Gate points set',
  GatePointSet = 'Gate point set',
  TimeStart = 'Time start',
  TimeEnd = 'Time end',
  TimeReset = 'Time reset'
}

export class PointEvent {
  
  public type: PointEventType;
  public value: number | string;
  public msg: string;

  constructor(type: PointEventType,
    value?: number | string,
    msg?: string){
    this.type = type;
    this.value = value;
    if (msg) {
      this.msg = msg;
    }
  }
  
}