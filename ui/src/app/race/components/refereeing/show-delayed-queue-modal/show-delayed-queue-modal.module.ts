import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatButtonModule, MatProgressSpinnerModule } from "@angular/material";
import { FlexModule } from "@angular/flex-layout";
import { ShowDelayedQueueModalComponent } from "./show-delayed-queue-modal.component";

@NgModule({
  declarations: [ShowDelayedQueueModalComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    FlexModule
  ],
  exports: [ShowDelayedQueueModalComponent],
  providers: [],
  entryComponents: [ShowDelayedQueueModalComponent]
})
export class ShowDelayedQueueModalModule {}
