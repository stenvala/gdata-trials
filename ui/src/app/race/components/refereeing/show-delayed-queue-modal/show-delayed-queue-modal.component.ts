import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { QueuingService } from "@race/services/queuing.service";
import { QueuingRider, SectionQueuer, QueueRider } from "@race/services/types";

export interface DialogData {
  section: number;
}

@Component({
  selector: "app-select-from-queue-modal",
  templateUrl: "./show-delayed-queue-modal.component.html",
  styleUrls: ["./show-delayed-queue-modal.component.scss"]
})
export class ShowDelayedQueueModalComponent implements OnInit {
  public loading: boolean = true;
  public queue: SectionQueuer[] = [];

  constructor(
    private queuingService: QueuingService,
    public dialogRef: MatDialogRef<ShowDelayedQueueModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: DialogData
  ) {}

  ngOnInit() {
    this.queuingService
      .fetchQueueOfSection(this.data.section)
      .subscribe(results => {
        this.loading = false;
        this.queue = results.sort((a, b) => (a.arrived < b.arrived ? -1 : 1));
      });
  }

  cancel(): void {
    this.dialogRef.close();
  }

  select(rider: QueueRider): void {
    this.dialogRef.close(rider);
  }
}
