import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from "@angular/core";
import { RaceService } from "../../services/race.service";
import { ActivatedRoute } from "@angular/router";
import { addSubscriptions } from "src/app/shared/utils/mobx-lifecycles";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { autorunner } from "src/app/shared/decorators/autorunner";
import {
  Definition,
  Category,
  Rider,
  EarlierSection,
  QueueRider
} from "../../services/types";
import { RefereeingService } from "./refereeing.service";
import { NavigationService } from "src/app/core/navigation/navigation.service";
import { PointEvent, PointEventType } from "./point-event.refereeing";
import { Subject } from "rxjs";
import { MatDialog } from "@angular/material";
import { StopwatchComponent } from "./stopwatch/stopwatch.component";
import { SelectFromQueueModalComponent } from "./select-from-queue-modal/select-from-queue-modal.component";
import { ToasterService } from "@shared/services/toaster.service";
import { ShowDelayedQueueModalComponent } from "./show-delayed-queue-modal/show-delayed-queue-modal.component";

@Component({
  selector: "app-race-refereeing",
  templateUrl: "./refereeing.component.html",
  styleUrls: ["./refereeing.component.scss"]
})
export class RefereeingComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;
  definition: Definition;
  loading: boolean = true;

  selectedSection: number | undefined;

  categories: Category[];
  selectedCategory: Category | undefined;

  riders: Rider[];
  selectedRider: Rider | undefined;

  laps: number[];

  selectedLap: number;
  ridersPointsForOngoingSection: number;
  ridersGatePointsForOngoingSection: number;

  resetGates: Subject<number> = new Subject<number>();

  clockReseted: boolean = true;

  earlierSections: EarlierSection[] = [];

  isSaving: boolean;

  private interval: any = undefined;
  @ViewChild("tosave", { static: false })
  set toSaveValue(value: ElementRef) {
    if (value) {
      value.nativeElement.innerHTML =
        this.refereeingService.secondsToNextSave + " s";
      this.interval = setInterval(() => {
        value.nativeElement.innerHTML =
          this.refereeingService.secondsToNextSave + " s";
      }, 500);
    } else if (this.interval) {
      clearInterval(this.interval);
      this.interval = undefined;
    }
  }

  private intervalAnimateSaving: any = undefined;
  private previous: number = 1;
  private next: number = 1;
  @ViewChild("animatesaving", { static: false })
  set toShowValue(value: ElementRef) {
    if (value) {
      const dots = Array(this.previous)
        .fill(".")
        .join("");
      value.nativeElement.innerHTML = dots + ".." + dots;
      this.intervalAnimateSaving = setInterval(() => {
        const dots = Array(this.previous)
          .fill(".")
          .join("");
        value.nativeElement.innerHTML = dots + ".." + dots;
        this.previous += this.next;
        if (this.previous === 5 || this.previous === -1) {
          this.next *= -1;
          this.previous += this.next;
        }
      }, 250);
    } else if (this.intervalAnimateSaving) {
      clearInterval(this.intervalAnimateSaving);
      this.intervalAnimateSaving = undefined;
    }
  }

  @ViewChild("stopwatch", { static: false })
  private stopwatch: StopwatchComponent;

  constructor(
    public raceService: RaceService,
    private activatedRoute: ActivatedRoute,
    public refereeingService: RefereeingService,
    private navigationService: NavigationService,
    private toasterService: ToasterService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    addSubscriptions(
      this,
      this.activatedRoute.params.subscribe(params => {
        let raceId = params["sheetId"];
        this.raceService.setRaceId(raceId);
      })
    );
    this.createRegisterForm();
    this.earlierSections = this.refereeingService.getEarlierSections();
  }

  ngOnDestroy() {
    this.navigationService.hideNavigation.next(false);
    this.refereeingService.setCategory(undefined);
  }

  @autorunner()
  private checkIfSaving() {
    this.isSaving = this.refereeingService.isSaving;
  }

  @autorunner()
  private setCompetition() {
    this.definition = this.raceService.definition;
    this.loading = !this.definition;
  }

  @autorunner()
  private setCategories() {
    this.selectedSection = this.refereeingService.selectedSection;
    this.categories = this.refereeingService.categories;
    if (this.categories.length == 1 && !this.selectedCategory) {
      this.selectCategory(this.categories[0]);
    }
  }

  @autorunner()
  private setSelectedCategory() {
    this.selectedCategory = this.refereeingService.selectedCategory;
  }

  @autorunner()
  private setRiders() {
    this.riders = this.refereeingService.riders
      .slice()
      .sort((a, b) => (a.number < b.number ? -1 : 1));
    if (this.riders.length === 1 && !this.selectedRider) {
      this.selectRider(this.riders[0]);
    }
  }

  @autorunner()
  private setSelectedRider() {
    let previousNumber: number | undefined = undefined;
    if (this.selectedRider && this.selectedRider.number) {
      previousNumber = this.selectedRider.number;
    }
    this.selectedRider = this.refereeingService.selectedRider;
    if (!this.selectedCategory || !this.selectedRider) {
      return;
    }
    if (!previousNumber || this.selectedRider.number != previousNumber) {
      let lap = 1;
      this.laps = [...Array(this.selectedCategory.laps + 1).keys()];
      this.laps.splice(0, 1);
      for (var i = this.selectedCategory.laps; i > 0; i--) {
        if (!(this.selectedSection in this.selectedRider.sectionResults[i])) {
          lap = i;
        }
      }
      this.selectedLap = lap;
      this.solvePointsForOngoingSection();
    }
  }

  public selectLap(lap: number) {
    if (!this.canDoChange()) {
      return;
    }
    this.selectedLap = lap;
    this.solvePointsForOngoingSection();
    if (this.selectedCategory.pointSystem == "POINTS") {
      this.resetGates.next(this.ridersGatePointsForOngoingSection);
    }
  }

  private solvePointsForOngoingSection() {
    const points =
      this.selectedSection in
      this.selectedRider.sectionResults[this.selectedLap]
        ? this.selectedRider.sectionResults[this.selectedLap][
            this.selectedSection
          ]
        : -1;
    switch (this.selectedCategory.pointSystem) {
      case "POINTS":
        this.ridersPointsForOngoingSection = 0;
        this.ridersGatePointsForOngoingSection = points < 0 ? 0 : points;
        break;
      case "PENALTIES":
        this.ridersPointsForOngoingSection = points;
        break;
    }
  }

  onStopwatchEvent(event: PointEvent) {
    if (
      event.type === PointEventType.TimeStart &&
      this.selectedCategory.pointSystem === "PENALTIES" &&
      this.ridersPointsForOngoingSection == -1
    ) {
      this.ridersPointsForOngoingSection = 0;
    }
    // Always save on clock event
    this.clockReseted = event.type === PointEventType.TimeReset;
    // save points. Can be gate rules and 0 points, or something else. Just in case.
    //if (this.clockReseted) {
    //  this.savePoints();
    //}
    if (event.type !== PointEventType.TimeReset) {
      this.savePoints();
    }
  }

  public clearPoints() {
    this.refereeingService.setPoints(
      this.selectedCategory,
      this.selectedRider,
      this.selectedLap,
      this.selectedSection
    );
    this.ridersGatePointsForOngoingSection = 0;
    this.ridersPointsForOngoingSection =
      this.selectedCategory.pointSystem === "POINTS" ? 0 : -1;
    this.resetGates.next(0);
  }

  private getPreviousTime() {
    if (this.stopwatch) {
      return this.stopwatch.getPreviousTime();
    }
    return "-";
  }

  private savePoints() {
    switch (this.selectedCategory.pointSystem) {
      case "PENALTIES":
        this.refereeingService.setPoints(
          this.selectedCategory,
          this.selectedRider,
          this.selectedLap,
          this.selectedSection,
          this.ridersPointsForOngoingSection,
          this.getPreviousTime()
        );
        break;
      case "POINTS":
        this.refereeingService.setPoints(
          this.selectedCategory,
          this.selectedRider,
          this.selectedLap,
          this.selectedSection,
          this.ridersGatePointsForOngoingSection,
          this.getPreviousTime()
        );
        break;
    }
  }

  onPointEvent(event: PointEvent) {
    switch (event.type) {
      case PointEventType.PenaltyPointsSet:
        this.ridersPointsForOngoingSection = event.value as number;
        break;
      case PointEventType.GatePointsSet:
        this.ridersGatePointsForOngoingSection = event.value as number;
        break;
    }
    if (
      (event.type === PointEventType.PenaltyPointsSet &&
        this.selectedCategory.pointSystem == "PENALTIES") ||
      (event.type === PointEventType.GatePointsSet &&
        this.selectedCategory.pointSystem == "POINTS")
    ) {
      this.savePoints();
    }
  }

  selectCategory(category: Category) {
    if (!this.canDoChange()) {
      return;
    }
    this.selectRider(undefined);
    if (category === this.selectedCategory) {
      if (this.categories.length !== 1) {
        this.refereeingService.setCategory(undefined);
      } else {
        this.toasterService.showErrorMessage(
          "Tällä jaksolla ajaa vain yksi luokka."
        );
      }
    } else {
      this.refereeingService.setCategory(category);
    }
  }

  selectRider(rider: Rider, forceSet: boolean = false) {
    if (!this.canDoChange()) {
      return;
    }
    if (rider === this.selectedRider && !forceSet) {
      if (this.riders.length !== 1) {
        this.refereeingService.setRider(undefined);
      } else {
        this.toasterService.showErrorMessage(
          "Tässä luokassa ajaa vain yksi kilpailija."
        );
      }
    } else {
      this.refereeingService.setRider(rider);
    }
  }

  showDelayedQueue() {
    const queue = this.refereeingService.pointsToSave.map(i => i);
    this.dialog.open(SelectFromQueueModalComponent, {
      width: window.innerWidth + "px",
      data: {
        queue: queue
      }
    });
  }

  selectFromQueue() {
    if (!this.canDoChange()) {
      return;
    }
    const dialogRef = this.dialog.open(ShowDelayedQueueModalComponent, {
      width: window.innerWidth + "px",
      data: {
        section: this.selectedSection
      }
    });
    dialogRef.afterClosed().subscribe((result?: QueueRider) => {
      if (result) {
        if (
          !this.selectedCategory ||
          this.selectedCategory.name !== result.category
        ) {
          const category = this.raceService.getCategory(result.category);
          this.selectCategory(category);
        }
        this.selectRider(undefined, true);
        this.refereeingService.riders.some((rider: Rider) => {
          if (rider.number === result.riderNumber) {
            setTimeout(() => {
              this.selectRider(rider, true);
              this.refereeingService
                .removeFromQueueWithCode(
                  result.category,
                  result.riderNumber,
                  result.riderName
                )
                .subscribe();
              return true;
            });
          }
          return false;
        });
      }
    });
  }

  private canDoChange() {
    if (!this.clockReseted) {
      this.toasterService.showErrorMessage("Nollaa kello ennen vaihtoa.");
      return false;
    }
    return true;
  }

  private setSection(section: number, code: string) {
    this.navigationService.hideNavigation.next(true);
    this.refereeingService.setSection(section, code);
  }

  resetSection() {
    if (!this.canDoChange()) {
      return;
    }
    this.refereeingService.setSection(undefined, undefined);
    this.selectCategory(undefined);
    this.selectRider(undefined);
    this.navigationService.hideNavigation.next(false);
  }

  selectEarlierSection(eSection: EarlierSection) {
    this.loading = true;
    this.refereeingService.getSectionForCode(eSection.code).subscribe(
      section => {
        if (section === eSection.section) {
          this.loading = false;
          this.setSection(section, eSection.code);
          this.earlierSections = this.refereeingService.addEarlierSection(
            eSection
          );
        } else {
          this.toasterService.showErrorMessage("Jaksokoodi on vaihtunut.");
          this.earlierSections = this.refereeingService.removeEarlierSection(
            eSection.section
          );
        }
      },
      error => {
        this.loading = false;
        this.toasterService.showErrorMessage("Jaksokoodi on vaihtunut.");
        this.earlierSections = this.refereeingService.removeEarlierSection(
          eSection.section
        );
      }
    );
  }

  testCode() {
    const code = this.registerForm.get("code").value;
    this.loading = true;
    this.refereeingService.getSectionForCode(code).subscribe(
      section => {
        this.loading = false;
        this.setSection(section, code);
        this.earlierSections = this.refereeingService.addEarlierSection({
          section: section,
          code: code
        });
      },
      error => {
        this.loading = false;
        this.toasterService.showErrorMessage("Virheellinen jaksokoodi.");
      }
    );
  }

  private createRegisterForm() {
    this.registerForm = new FormGroup({
      code: new FormControl("", [Validators.required, Validators.maxLength(60)])
    });
  }
}
