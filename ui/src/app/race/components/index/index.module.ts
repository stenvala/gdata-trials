import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule, MatCardModule, MatButtonModule, MatMenuModule } from '@angular/material';
import { ApiService } from '../../services/api.service';
import { RaceService } from '../../services/race.service';
import { IndexComponent } from './index.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [    
    IndexComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatButtonModule,
    MatMenuModule    
  ],
  exports: [
    IndexComponent
  ],
  providers: [    
    RaceService,
    ApiService    
  ]
})
export class IndexModule { }
