import { Component, OnInit, OnDestroy } from "@angular/core";
import { RaceService } from "../../services/race.service";
import { Race } from "../../services/types";
import { createTransformer } from "mobx-utils";
import { UserService } from "@core/services/user.service";
import { autorunner } from "@shared/decorators/autorunner";
import { ToasterService } from "@shared/services/toaster.service";

@Component({
  selector: "app-race-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class IndexComponent implements OnInit, OnDestroy {
  races: Race[];
  showBoosters: boolean;

  constructor(
    public raceService: RaceService,
    private userService: UserService,
    private toasterService: ToasterService
  ) {}

  ngOnInit() {
    this.raceService.getRaces().then(races => (this.races = races));
  }

  ngOnDestroy() {}

  @autorunner()
  private setShowBoosters() {
    this.showBoosters = this.userService.isLoggedIn;
  }

  sectionCards(race: Race) {
    var url =
      location.protocol +
      "//" +
      location.host +
      "/race/" +
      race.id +
      "/section-cards";
    let win = window.open(url);
    win.location.href = url;
    win.location.reload();
  }

  showMenu = createTransformer(
    (race: Race): boolean =>
      race.sectionCards || race.refereeing || race.queuing
  );

  resetCache(raceId: string) {
    this.raceService.setRaceId(raceId, true);
  }
  saveSelectedRiderData(raceId: string) {
    this.raceService
      .saveSelectedRiderData(raceId)
      .subscribe(
        this.toasterService.getSubscribeCallback(
          "Tiedot tallennettu onnistuneesti."
        )
      );
  }
  downsyncSelectedRiderData(raceId: string) {
    this.raceService
      .downsyncSelectedRiderData(raceId)
      .subscribe(
        this.toasterService.getSubscribeCallback(
          "Tiedot ladattu onnistuneesti."
        )
      );
  }
}
