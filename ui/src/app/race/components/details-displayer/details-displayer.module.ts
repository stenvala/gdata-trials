import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DetailsDisplayerComponent } from './details-displayer.component';

@NgModule({
  declarations: [    
    DetailsDisplayerComponent
  ],
  imports: [
    CommonModule,    
  ],
  exports: [
    DetailsDisplayerComponent
  ]
})
export class DetailsDisplayerModule { }
