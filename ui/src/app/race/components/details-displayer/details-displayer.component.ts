import { Component, OnInit, Input } from '@angular/core';
import { Definition } from '../../services/types';
import { toJS } from 'mobx';

@Component({
  selector: 'app-race-details-displayer',
  templateUrl: './details-displayer.component.html',
  styleUrls: ['./details-displayer.component.scss']
})
export class DetailsDisplayerComponent implements OnInit {

  @Input() definition: Definition;
  
  constructor() { }

  ngOnInit() {        
  }

}
