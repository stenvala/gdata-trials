import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  MatProgressSpinnerModule,
  MatCardModule,
  MatButtonModule
} from "@angular/material";

import { DetailsDisplayerModule } from "../details-displayer/details-displayer.module";
import { ResultsComponent } from "./results.component";
import { RiderResultsComponent } from "./rider-results/rider-results.component";
import { FlexModule } from "@angular/flex-layout";

@NgModule({
  declarations: [ResultsComponent, RiderResultsComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatButtonModule,
    DetailsDisplayerModule,
    FlexModule
  ],
  exports: [RiderResultsComponent]
})
export class ResultsModule {}
