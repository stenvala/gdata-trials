import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RaceService } from "../../services/race.service";
import { addSubscriptions } from "src/app/shared/utils/mobx-lifecycles";
import { ControlStateService } from "@core/services/control-state.service";

@Component({
  selector: "app-race-results",
  templateUrl: "./results.component.html",
  styleUrls: ["./results.component.scss"]
})
export class ResultsComponent implements OnInit, OnDestroy {
  static LS_KEY = "RESULTS_COMPONENT_HIDE_SECTION_DETAILS";

  hideSectionResults: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    public raceService: RaceService,
    private controlStateService: ControlStateService
  ) {}

  ngOnInit() {
    addSubscriptions(
      this,
      this.activatedRoute.params.subscribe(params => {
        let raceId = params["sheetId"];
        this.raceService.setRaceId(raceId);
      })
    );
    this.hideSectionResults = this.controlStateService.get(
      ResultsComponent.LS_KEY,
      { value: false }
    ).value;
  }

  toggleSectionResults() {
    this.hideSectionResults = !this.hideSectionResults;
    this.controlStateService.set(ResultsComponent.LS_KEY, {
      value: this.hideSectionResults
    });
  }

  ngOnDestroy() {}

  refresh() {
    this.raceService.refreshAll();
  }
}
