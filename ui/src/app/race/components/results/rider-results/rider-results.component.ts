import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy
} from "@angular/core";
import { Category, RiderFullResult } from "@race/services/types";

@Component({
  selector: "app-rider-results",
  templateUrl: "./rider-results.component.html",
  styleUrls: ["./rider-results.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RiderResultsComponent implements OnInit {
  @Input()
  rider: RiderFullResult;

  @Input()
  category: Category;

  @Input()
  hideSectionResults: boolean;

  riderName: string;

  objectKeys = Object.keys;

  constructor() {}

  ngOnInit() {}

  getSectionResult(
    section: number,
    results: { [key: number]: number }
  ): string | number {
    return section in results ? results[section] : "-";
  }
}
