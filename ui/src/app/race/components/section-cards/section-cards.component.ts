import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy
} from "@angular/core";
import { RaceService } from "../../services/race.service";
import { ActivatedRoute } from "@angular/router";
import {
  SectionCardsService,
  SectionCardsConfig
} from "./section-cards.service";
import { when } from "mobx";
import { NavigationService } from "src/app/core/navigation/navigation.service";
import { addSubscriptions } from "src/app/shared/utils/mobx-lifecycles";

@Component({
  selector: "app-race-section-cards",
  templateUrl: "./section-cards.component.html",
  styleUrls: ["./section-cards.component.scss"]
})
export class SectionCardsComponent implements OnInit, OnDestroy {
  public isReady: boolean;

  public useCategoryColors: boolean = true;

  @ViewChild("iframe", { static: true }) iframeElement: ElementRef;

  constructor(
    private raceService: RaceService,
    private activatedRoute: ActivatedRoute,
    private sectionCardsService: SectionCardsService,
    private navigationService: NavigationService
  ) {}

  ngOnInit() {
    this.navigationService.hideNavigation.next(true);
    addSubscriptions(
      this,
      this.activatedRoute.params.subscribe(params => {
        let raceId = params["sheetId"];
        this.raceService.setRaceId(raceId);
      })
    );
    this.create();
  }

  ngOnDestroy() {
    this.navigationService.hideNavigation.next(false);
  }

  async reset() {
    this.isReady = false;
    this.raceService.refreshAll(true);
    this.create();
  }

  private getConfig(): SectionCardsConfig {
    return {
      useCategoryColors: this.useCategoryColors
    };
  }

  changeColorToggle() {
    this.useCategoryColors = !this.useCategoryColors;
    this.create();
  }

  private create() {
    this.iframeElement.nativeElement.style.visibility = "hidden";
    this.isReady = false;
    when(
      () => this.raceService.loaded,
      () => {
        let doc = this.sectionCardsService.getCardsPdf(this.getConfig());
        let data = doc.output("datauristring");
        this.iframeElement.nativeElement.setAttribute("src", data);
        setTimeout(() => {
          let el = this.iframeElement.nativeElement;
          el.style.visibility = "visible";
          el.style.height =
            (
              window.innerHeight -
              el.getBoundingClientRect().top -
              5
            ).toString() + "px";
          this.isReady = true;
        }, 1000);
      }
    );
  }
}
