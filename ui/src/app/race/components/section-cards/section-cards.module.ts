import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  MatProgressSpinnerModule,
  MatCardModule,
  MatButtonModule
} from "@angular/material";
import { RouterModule } from "@angular/router";

import { DetailsDisplayerModule } from "../details-displayer/details-displayer.module";
import { SectionCardsComponent } from "./section-cards.component";

@NgModule({
  declarations: [SectionCardsComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatButtonModule,
    DetailsDisplayerModule
  ],
  providers: []
})
export class SectionCardsModule {}
