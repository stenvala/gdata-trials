import { Injectable } from "@angular/core";

import "jspdf-autotable";
import * as jsPDF from "jspdf";
import { RaceService } from "../../services/race.service";
import { Definition, Category, Rider } from "../../services/types";

export interface SectionCardsConfig {
  useCategoryColors: boolean;
}

@Injectable({
  providedIn: "root"
})
export class SectionCardsService {
  constructor(private raceService: RaceService) {}

  getCardsPdf(config: SectionCardsConfig): jsPDF {
    let doc = new jsPDF("l", "mm");
    doc.setFontSize(12);
    doc.setTextColor(0);
    let results = this.raceService.allResults;
    const xInit = 8;
    const yInit = 8;
    const width = (297 - 2 * xInit) / 5;
    const minCellHeight =
      (((210 - 2 * yInit) / 2 / 15) *
        1.15 *
        this.raceService.definition.sectionCardRowHeightScale) /
      100;
    let x = xInit;
    let y = yInit;
    let currentCard = 0;
    Object.keys(results).forEach(key => {
      let cat = this.raceService.getCategory(key);
      results[key].forEach((result: Rider) => {
        for (let lap = 1; lap <= cat.laps; lap++) {
          if (currentCard == 10) {
            currentCard = 0;
            x = xInit;
            y = yInit;
            doc.addPage();
          }
          if (currentCard == 5) {
            x = xInit;
            y = 210 / 2;
          }
          currentCard++;
          this.createCardForRider(
            doc,
            x,
            y,
            this.raceService.definition,
            cat,
            result,
            lap,
            width,
            minCellHeight,
            config
          );
          x = x + (297 - xInit * 2) / 5;
        }
      });
    });
    return doc;
  }

  private createCardForRider(
    doc: jsPDF,
    x: number,
    y: number,
    def: Definition,
    cat: Category,
    rider: Rider,
    lap: number,
    width: number,
    minCellHeight: number,
    config: SectionCardsConfig
  ) {
    let cols = cat.pointSystem == "POINTS" ? 8 : 7;
    let id =
      "sectionCardRandomId_" + Math.round(Math.random() * 10000).toString();
    let html = '<table id="' + id + '">';
    // Race data
    let gray = "200,200,200";
    let bgColor = config.useCategoryColors
      ? cat.color.R + "," + cat.color.G + "," + cat.color.B
      : gray;

    html +=
      '<tr><td colspan="' +
      cols +
      '" style="text-align: center; border: 1px solid black; background-color: rgb(' +
      bgColor +
      ');">' +
      def.title +
      "<br>" +
      def.date +
      "<br>" +
      def.location +
      "</td></tr>\n";
    // Rider
    html +=
      '<tr><td colspan="' +
      cols +
      '" style="text-align: center; border: 1px solid black;">' +
      rider.riderData[def.riderColumnNames[0]] +
      (rider.riderData[def.riderColumnNames[0]] != ""
        ? " #<b>" + rider.number + "</b>"
        : "") +
      "</td></tr>\n";
    // Category and lap
    html +=
      '<tr><td colspan="4" style="text-align: center; border: 1px solid black;">' +
      (rider.riderData[def.riderColumnNames[0]] != "" ? cat.name : "") +
      "</td>" +
      '<td colspan="' +
      (cols - 4) +
      '" style="text-align: center; border: 1px solid black;">' +
      def.translations["Lap"] +
      ": <b>" +
      lap.toString() +
      "</b>/" +
      cat.laps.toString() +
      "</td></tr>";
    // Points
    html += '<tr><td style="border: 1px solid black;"></td>';
    let pointRow = "";
    for (var i = 0; i < 6; i++) {
      html +=
        '<td style="text-align: center; border: 1px solid black;">' +
        (cat.pointSystem == "POINTS" ? i + 1 : i) +
        "</td>";
      pointRow += '<td style="border: 1px solid black;"></td>';
    }
    if (cat.pointSystem == "POINTS") {
      html +=
        '<td style="text-align: center; border: 1px solid black;">' +
        def.translations["Total"] +
        "</td>";
      pointRow += '<td style="border: 1px solid black;"></td>';
    }
    html += "</tr>";
    // Sections
    cat.sections.forEach(s => {
      html +=
        '<tr><td style="text-align: center; border: 1px solid black;">' +
        s +
        "</td>" +
        pointRow +
        "</tr>";
    });
    // Time
    if (lap != cat.laps) {
      html +=
        '<tr><td colspan="' +
        cols +
        '" style="text-align: center; border: 1px solid black; background-color: rgb(' +
        gray +
        ');">' +
        def.organizer +
        "</td></tr>";
    } else {
      html +=
        "<tr>" +
        '<td colspan="4" style="text-align: center; border: 1px solid black; background-color: rgb(' +
        gray +
        ');">' +
        def.translations["Time"] +
        "</td>" +
        '<td style="border: 1px solid black;" colspan="' +
        (cols - 4) +
        '"></td>' +
        "</tr>";
    }
    // Tot
    html +=
      "<tr>" +
      '<td colspan="4" style="text-align: center; border: 1px solid black; background-color: rgb(' +
      gray +
      ');">' +
      def.translations["Total"] +
      "</td>" +
      '<td style="border: 1px solid black;" colspan="' +
      (cols - 4) +
      '"></td>' +
      "</tr>";
    html += "</table>";

    let div = document.createElement("div");
    div.innerHTML = html;
    const body = document.getElementsByTagName("body")[0];
    body.appendChild(div);

    //((doc as any).autoTable as AutoTable)({
    (doc as any).autoTable({
      styles: {
        fillColor: [255, 255, 255],
        minCellHeight: minCellHeight,
        lineWidth: 0.2,
        lineColor: 10
      },
      startY: y,
      html: "#" + id,
      theme: "plain",
      useCss: true, // this is missing in autoTable type definitions
      tableWidth: width,
      margin: { left: x, top: 0, bottom: 0, right: 0 },
      tableLineColor: 0,
      tableLineWidth: 0.75
    });
    div.remove();
  }
}
