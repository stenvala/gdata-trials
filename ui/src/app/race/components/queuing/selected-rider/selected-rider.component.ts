import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy
} from "@angular/core";
import {
  QueueRider,
  QueuingRider,
  Category,
  RiderFullResult
} from "@race/services/types";
import { RaceService } from "@race/services/race.service";
import { QueuingService } from "../../../services/queuing.service";
import { forkJoin } from "rxjs";
import { ToasterService } from "@shared/services/toaster.service";
import { autorunner } from "@shared/decorators/autorunner";

@Component({
  selector: "app-queuing-selected-rider",
  templateUrl: "./selected-rider.component.html",
  styleUrls: ["./selected-rider.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectedRiderComponent implements OnInit, OnDestroy {
  @Input()
  rider: QueueRider;
  raceId: string;
  queueResult: QueuingRider;
  result: RiderFullResult;
  lap: number;
  loading: boolean = true;
  availableSections: { section: number; queue: number }[] = [];
  category: Category;
  private queueLength: { [key: string]: number };
  constructor(
    private queueService: QueuingService,
    private raceService: RaceService,
    private cdr: ChangeDetectorRef,
    private toasterService: ToasterService
  ) {}

  ngOnInit() {
    this.category = this.raceService.getCategory(this.rider.category);
    this.reload();
  }

  ngOnDestroy() {}

  @autorunner()
  private setRaceId() {
    this.raceId = this.raceService.raceId;
  }

  reload() {
    this.loading = true;
    this.cdr.markForCheck();
    forkJoin({
      result: this.raceService.fetchRiderResultsByQueueCode(this.rider.code),
      queue: this.queueService.fetchQueueLengths()
    }).subscribe(response => {
      this.queueResult = response.result;
      (this.result = this.raceService.getRiderFullResultOf(
        this.category.name,
        response.result.number
      )),
        (this.queueLength = response.queue);
      this.loading = false;
      this.solveLapAndAvailableSections();
      this.cdr.markForCheck();
    });
  }

  selectSection(section: number) {
    this.loading = true;
    this.queueService.queueToSection(this.rider, section).subscribe(
      () => {
        this.reload();
      },
      () => {
        this.loading = false;
        this.toasterService.showErrorMessage("Jonoon siirtyminen epäonnistui");
      }
    );
  }

  leaveQueue() {
    this.loading = true;
    this.queueService.removeRiderFromQueue(this.rider).subscribe(
      () => {
        this.reload();
      },
      () => {
        this.reload();
      }
    );
  }

  private getQueueLengthForSection(section: number) {
    const key = section + "";
    return key in this.queueLength ? this.queueLength[key] : 0;
  }

  private solveLapAndAvailableSections() {
    this.lap = 0;
    for (let i = 1; i < this.category.laps; i++) {
      var missed = false;
      this.availableSections = [];
      this.category.sections.forEach(s => {
        if (!(s in this.queueResult.sectionResults[i])) {
          missed = true;
          if (s !== this.queueResult.queueInSection) {
            const obj = {
              section: s,
              queue: this.getQueueLengthForSection(s)
            };
            this.availableSections.push(obj);
          }
        }
      });
      if (missed) {
        this.lap = i;
        break;
      }
    }
  }
}
