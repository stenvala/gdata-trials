import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from "@angular/core";
import { RaceService } from "../../services/race.service";
import { autorunner } from "src/app/shared/decorators/autorunner";
import { ActivatedRoute } from "@angular/router";
import { SimpleRider, QueueRider } from "../../services/types";
import { QueuingService } from "../../services/queuing.service";
import { ToasterService } from "@shared/services/toaster.service";
import { ErrorDTO } from "@core/error-type";

@Component({
  selector: "app-queuing",
  templateUrl: "./queuing.component.html",
  styleUrls: ["./queuing.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QueuingComponent implements OnInit, OnDestroy {
  showRiderList: boolean = true;
  riders: SimpleRider[];
  myRiders: QueueRider[];
  selectedRider: QueueRider | undefined = undefined;

  private loadingRaces: boolean = false;
  private settingToQueue: boolean = false;
  private releasingRider: boolean = false;
  // fill all loading statuses inside here
  get loading(): boolean {
    return this.loadingRaces || this.settingToQueue || this.releasingRider;
  }

  constructor(
    private raceService: RaceService,
    private activatedRoute: ActivatedRoute,
    private queuingService: QueuingService,
    private cdr: ChangeDetectorRef,
    private toasterService: ToasterService
  ) {}

  ngOnInit() {
    this.raceService.setRaceId(this.activatedRoute.snapshot.params.sheetId);
  }

  ngOnDestroy() {}

  @autorunner()
  private checkLoading() {
    this.loadingRaces = !this.raceService.loaded;
    this.cdr.markForCheck();
  }

  @autorunner()
  private setRaces() {
    this.riders = this.raceService.simpleRiderList;
    this.cdr.markForCheck();
  }

  @autorunner()
  private setMyRiders() {
    this.myRiders = this.queuingService.myRiders;
    this.cdr.markForCheck();
  }

  unselectRider() {
    this.selectedRider = undefined;
  }

  selectMyRider(rider: QueueRider) {
    this.selectedRider = rider;
    this.cdr.markForCheck();
  }

  selectRider(rider: SimpleRider) {
    this.settingToQueue = true;
    this.cdr.markForCheck();
    const cb = () => {
      this.settingToQueue = false;
      this.cdr.markForCheck();
    };
    this.queuingService
      .takeRider(rider.category, rider.number, rider.name)
      .subscribe(
        this.toasterService.getSubscribeCallback(
          "Kuski otettu hallintaa.",
          (error: ErrorDTO): string => {
            console.log(error);
            switch (error.kind) {
              case "RIDER_PICK_NOT_AVAILABLE":
                return "Joku on jo ottanut kuskin hallintaan. Ota yhteys kisatoimistoon";
            }
            return "Tuntematon virhe kuskin hallintaanottamisessa";
          },
          cb,
          cb
        )
      );
  }

  releaseMyRider(rider: QueueRider) {
    this.releasingRider = true;
    this.cdr.markForCheck();
    const cb = () => {
      this.releasingRider = false;
      this.cdr.markForCheck();
    };
    this.queuingService
      .releaseRider(rider)
      .subscribe(
        this.toasterService.getSubscribeCallback(
          "Kuski vapautettu",
          undefined,
          cb,
          cb
        )
      );
  }
}
