import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  MatCardModule,
  MatProgressSpinnerModule,
  MatButtonModule,
  MatSnackBarModule
} from "@angular/material";

import { QueuingComponent } from "./queuing.component";
import { SharedModule } from "@shared/shared.module";
import { SelectedRiderComponent } from "./selected-rider/selected-rider.component";
import { ResultsModule } from "../results/results.module";
import { SectionQueueModalComponent } from "./section-queue-modal/section-queue-modal.component";
import { FlexModule } from "@angular/flex-layout";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [
    QueuingComponent,
    SelectedRiderComponent,
    SectionQueueModalComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatSnackBarModule,
    SharedModule,
    ResultsModule,
    FlexModule,
    RouterModule
  ]
})
export class QueuingModule {}
