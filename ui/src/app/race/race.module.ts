import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatProgressSpinnerModule,
  MatCardModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatSlideToggleModule
} from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout";
import { RaceRoutingModule } from "./race-routing.module";

import { IndexModule } from "./components/index/index.module";
import { RefereeingModule } from "./components/refereeing/refereeing.modules";

import { DetailsDisplayerModule } from "./components/details-displayer/details-displayer.module";

import { QueuingModule } from "./components/queuing/queuing.module";
import { ResultsModule } from "./components/results/results.module";
import { SectionCardsModule } from "./components/section-cards/section-cards.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    FlexLayoutModule,
    RaceRoutingModule,
    IndexModule,
    RefereeingModule,
    DetailsDisplayerModule,
    QueuingModule,
    ResultsModule,
    SectionCardsModule
  ],
  providers: []
})
export class RaceModule {}
