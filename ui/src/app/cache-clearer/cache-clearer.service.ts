import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class CacheClearerService {
  private baseUrl: string = "/api/race";

  constructor(private http: HttpClient) {}

  clearAll() {
    return this.http.get<any>(this.baseUrl + "/clear");
  }

  clearIndex() {
    return this.http.get<any>(this.baseUrl + "/index/clear");
  }

  clearRaces() {
    return this.http.get<any>(this.baseUrl + "/clear-races");
  }
}
