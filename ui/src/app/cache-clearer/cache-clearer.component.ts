import { Component, OnInit } from "@angular/core";

import { CacheClearerService } from "./cache-clearer.service";
import { Observable } from "rxjs";
import { ToasterService } from "@shared/services/toaster.service";

@Component({
  selector: "app-cache-clearer",
  templateUrl: "./cache-clearer.component.html",
  styleUrls: ["./cache-clearer.component.scss"]
})
export class CacheClearerComponent implements OnInit {
  disabled: boolean = false;

  constructor(
    private cacheClearerService: CacheClearerService,
    private toasterService: ToasterService
  ) {}

  ngOnInit() {}

  clearAll() {
    this.clear(
      this.cacheClearerService.clearAll(),
      "Kaikki cachet tyhjennetty.",
      "Cachejen tyhjennys epäonnistui."
    );
  }

  clearIndex() {
    this.clear(
      this.cacheClearerService.clearIndex(),
      "Indeks tyhjennetty.",
      "Indeksin tyhjennys epäonnistui."
    );
  }

  clearRaces() {
    this.clear(
      this.cacheClearerService.clearRaces(),
      "Kilpailut tyhjennetty.",
      "Kilpailuiden tyhjennys epäonnistui."
    );
  }

  private clear(
    observable: Observable<any>,
    onSuccessMsg: string,
    onErrorMsg: string
  ) {
    this.disabled = true;
    let cb = () => (this.disabled = false);
    observable.subscribe(
      this.toasterService.getSubscribeCallback(onSuccessMsg, onErrorMsg, cb, cb)
    );
  }
}
