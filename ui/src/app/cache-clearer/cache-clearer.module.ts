import { NgModule } from "@angular/core";
import {
  MatCardModule,
  MatProgressSpinnerModule,
  MatButtonModule
} from "@angular/material";
import { CommonModule } from "@angular/common";

import { CacheClearerRoutingModule } from "./cache-clearer-routing.module";
import { CacheClearerService } from "./cache-clearer.service";

@NgModule({
  declarations: [CacheClearerRoutingModule.components],
  imports: [
    CommonModule,
    CacheClearerRoutingModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatButtonModule
  ],
  providers: [CacheClearerService]
})
export class CacheClearerModule {}
