import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CacheClearerComponent } from './cache-clearer.component';

const routes: Routes = [
  {path: '', component: CacheClearerComponent}
]

@NgModule({  
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CacheClearerRoutingModule { 
  static components = [ CacheClearerComponent ]
}
