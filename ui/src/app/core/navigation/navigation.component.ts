import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnDestroy
} from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";

import { filter } from "rxjs/operators";

import { NavigationService } from "./navigation.service";
import { Link } from "./link";
import { autorunner } from "@shared/decorators/autorunner";

@Component({
  selector: "app-core-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationComponent implements OnInit, OnDestroy {
  showNavigation = true;
  links: Link[] = [];
  activeTab: number = -1;
  reloadOnClick = true;

  constructor(
    public cdr: ChangeDetectorRef,
    private router: Router,
    private navigationService: NavigationService
  ) {}

  private url: string = window.location.pathname;

  ngOnInit() {
    this.navigationService.hideNavigation.subscribe(value => {
      this.showNavigation = !value;
      this.cdr.detectChanges();
    });

    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((value: NavigationEnd) => {
        this.url = value.url;
        this.solveActiveTab();
      });
  }

  private solveActiveTab() {
    let tab = this.links.findIndex(
      link => this.url.indexOf(link.base ? link.base : link.link) === 0
    );
    if (tab !== -1) {
      this.activeTab = tab;
    }
  }

  ngOnDestroy() {}

  @autorunner()
  private resetLinks() {
    this.links = this.navigationService.links;
    this.solveActiveTab();
    this.cdr.detectChanges();
  }

  goto(link: string) {
    this.router.navigate([link]);
  }

  tabChanged(index: number) {
    if (index === this.activeTab) {
      return;
    }
    this.goto(this.links[index].link);
    this.reloadOnClick = false;
  }

  clickTabGroup() {
    if (this.reloadOnClick) {
      this.router.navigate([this.links[this.activeTab].link]);
    }
    this.reloadOnClick = true;
  }
}
