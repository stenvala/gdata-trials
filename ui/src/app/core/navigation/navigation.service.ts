import { Injectable, OnInit, OnDestroy } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { Link } from "./link";
import { observable, computed } from "mobx-angular";
import { UserService } from "@core/services/user.service";
import { autorunner } from "@shared/decorators/autorunner";
import { ThrowStmt } from "@angular/compiler";

@Injectable({
  providedIn: "root"
})
export class NavigationService {
  // anyone can next and subscribe this
  public hideNavigation = new BehaviorSubject<boolean>(false);

  constructor(private userService: UserService) {}

  @computed
  get links(): Link[] {
    const isLoggedIn = this.userService.isLoggedIn;
    let links: Link[] = this.getBasicLinks();
    if (isLoggedIn) {
      return links.concat(this.getPrivateLinks());
    }
    return links.concat([this.getLoginLink()]);
  }

  private getBasicLinks(): Link[] {
    let links: Link[] = [];
    links.push({
      icon: "fa-home",
      link: "/home",
      label: "Etusivu"
    });
    links.push({
      icon: "fa-list",
      link: "/race/index",
      base: "/race",
      label: "Kilpailut"
    });
    return links;
  }

  private getLoginLink(): Link {
    return {
      icon: "fa-user",
      link: "/login",
      label: "Lisätoiminnot"
    };
  }

  private getPrivateLinks(): Link[] {
    return [
      {
        icon: "fa-cogs",
        link: "/clear",
        label: "Cache clearer"
      }
    ];
  }
}
