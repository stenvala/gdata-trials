export interface Link {
  icon: string;
  link: string;
  label: string;
  base?: string;
}
