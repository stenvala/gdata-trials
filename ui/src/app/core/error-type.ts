export type ErrorType =
  | "RIDER_RELEASE_IN_QUEUE"
  | "RIDER_PICK_NOT_AVAILABLE"
  | "UNDEFINED";

export interface ErrorDTO {
  data: any;
  kind: ErrorType;
  message: string;
}
