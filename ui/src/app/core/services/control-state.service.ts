import { Injectable } from "@angular/core";

import { keys, toJS } from "mobx";

@Injectable({
  providedIn: "root"
})
export class ControlStateService {
  private prefix = "ControlState";

  public set(key: string, dataObj: any): void {
    localStorage.setItem(this.prefix + key, JSON.stringify(toJS(dataObj)));
  }

  public get<T>(key: string, defaultObj: T): T {
    let val = localStorage.getItem(this.prefix + key);
    if (!val) {
      return defaultObj;
    }
    return JSON.parse(val) as T;
  }

  public remove(key: string) {
    localStorage.removeItem(this.prefix + key);
  }

  public clear() {
    let keysToRemove: string[] = [];
    for (let i = 0; i < localStorage.length; i++) {
      let key = localStorage.key(i);
      if (key.search(this.prefix) === 0) {
        keysToRemove.push(key);
      }
    }
    if (keys.length === localStorage.length) {
      localStorage.clear();
    } else {
      keysToRemove.forEach(key => this.remove(key));
    }
  }
}
