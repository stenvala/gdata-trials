import { Injectable } from "@angular/core";
import { ControlStateService } from "./control-state.service";
import { observable, action } from "mobx-angular";

interface Session {
  loggedIn?: boolean;
}

@Injectable({
  providedIn: "root"
})
export class UserService {
  static LS_KEY = "USER_SERVICE_LOGGED_IN";

  @observable
  isLoggedIn: boolean = false;

  constructor(private controlStateService: ControlStateService) {
    const session = this.controlStateService.get<Session>(
      UserService.LS_KEY,
      {}
    );
    if (session.loggedIn) {
      this.isLoggedIn = true;
    }
  }

  @action
  setLoginStatus(value: boolean) {
    this.isLoggedIn = value;
    if (value) {
      this.controlStateService.set(UserService.LS_KEY, {
        loggedIn: true
      });
    } else {
      this.controlStateService.remove(UserService.LS_KEY);
    }
  }
}
