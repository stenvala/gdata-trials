import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { UserService } from "@core/services/user.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.createLoginForm();
  }

  testCode() {
    const code = this.loginForm.get("code").value;
    if (code === "Keijo") {
      this.userService.setLoginStatus(true);
    }
  }

  private createLoginForm() {
    this.loginForm = new FormGroup({
      code: new FormControl("", [Validators.required, Validators.maxLength(60)])
    });
  }
}
