import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { ErrorDTO } from "@core/error-type";
import { HttpErrorResponse } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ToasterService {
  constructor(private snackBar: MatSnackBar) {}

  getSubscribeCallback<T>(
    okMessage: string | ((result: T) => void | string),
    errorMessage?: string | ((error: ErrorDTO) => void | string),
    okCallback?: (result: T) => void,
    errorCallback?: (error: ErrorDTO) => void
  ) {
    return {
      next: (result: T) => {
        if (okMessage && typeof okMessage === "string") {
          this.showOkMessage(okMessage);
        } else if (typeof okMessage === "function") {
          const reply = okMessage(result);
          if (reply) {
            this.showOkMessage(reply);
          }
        }
        if (okCallback) {
          okCallback(result);
        }
      },
      error: (error: HttpErrorResponse) => {
        if (errorMessage && typeof errorMessage === "string") {
          this.showErrorMessage(errorMessage);
        } else if (typeof errorMessage === "function") {
          const reply = errorMessage(error.error);
          if (reply) {
            this.showOkMessage(reply);
          }
        }
        if (errorCallback) {
          errorCallback(error.error);
        }
      }
    };
  }

  showOkMessage(msg: string, duration: number = 2000) {
    this.showMessage(msg, duration, "OK!");
  }
  showErrorMessage(msg: string, duration: number = 2000) {
    this.showMessage(msg, duration, "ERROR!");
  }

  showMessage(msg: string, duration: number, type: string) {
    this.snackBar.open(msg, type, {
      duration: duration
    });
  }
}
