import { OnDestroy } from "@angular/core";

let subs: WeakMap<OnDestroy, (() => void)[]> = new Map();
let addedAutodestroyers: WeakMap<OnDestroy, boolean> = new Map();

// all things used here are automatically disposed

// add with this subscriptions
export function addSubscriptions<T extends { unsubscribe: () => void }>(
  component: OnDestroy,
  ...subscriptions: T[]
) {
  if (!subs.has(component)) {
    subs.set(component, []);
  }
  subscriptions.forEach(s => subs.get(component).push(() => s.unsubscribe()));
  addAutodestroyers(component);
}

// add disposers with this
export function addDisposers(
  component: OnDestroy,
  ...disposers: (() => void)[]
) {
  if (!subs.has(component)) {
    subs.set(component, []);
  }
  disposers.forEach(s => subs.get(component).push(s));
  addAutodestroyers(component);
}

// call this if you need to manually stop disposers
export function stopSubscriptions(component: OnDestroy) {
  if (subs.has(component)) {
    subs.get(component).forEach(c => c());
    subs.delete(component);
  }
}

// adds disposers
function addAutodestroyers(component: OnDestroy) {
  if (addedAutodestroyers.has(component)) {
    return;
  }
  addedAutodestroyers.set(component, true);
  const originalDestroy = component.ngOnDestroy;
  component.ngOnDestroy = function() {
    if (originalDestroy) {
      originalDestroy.apply(this);
    }
    stopSubscriptions(component);
    addedAutodestroyers.delete(component);
  };
}
