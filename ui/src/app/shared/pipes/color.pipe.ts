import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "color" })
export class ColorPipe implements PipeTransform {
  transform(color: { R: number; G: number; B: number }): string {
    return "rgb(" + color.R + "," + color.G + "," + color.B + ")";
  }
  s;
}
