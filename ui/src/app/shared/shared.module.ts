import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ColorPipe } from "./pipes/color.pipe";
import { MatSnackBarModule } from "@angular/material";

@NgModule({
  imports: [CommonModule, MatSnackBarModule],
  declarations: [ColorPipe],
  exports: [ColorPipe]
})
export class SharedModule {}
