import { autorun } from "mobx";

import { addDisposers } from "../utils/mobx-lifecycles";
import { OnDestroy, OnInit } from "@angular/core";

// use decorator @autorunner for method to mobx autorun function and dispose automatically when component is destroyed
export function autorunner() {
  return function(
    target: OnInit & OnDestroy,
    key: string | symbol,
    descriptor: PropertyDescriptor
  ): PropertyDescriptor {
    const originalMethod = descriptor.value;
    const originalInit = target.ngOnInit;
    // wrap all decorated functions to ngOnInit
    target.ngOnInit = function() {
      if (originalInit) {
        originalInit.apply(this);
      }
      let meth = () => autorun(originalMethod.bind(this));
      addDisposers(target, meth());
    };
    return descriptor;
  };
}
